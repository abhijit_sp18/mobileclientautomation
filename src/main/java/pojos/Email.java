package pojos;

import java.util.List;

public class Email {

    private List<String> to;
    private List<String> cc;
    private List<String> bcc;
    private String subject;
    private String body;
    private Enum priority;

    enum Priorities{HIGH, MEDIUM, LOW;}

    public Email(List<String > to, String subject, String body){
        this.to = to;
        this.subject = subject;
        this.body = body;
    }

    public void setTo(List<String> to){ this.to = to; }

    public void setCc(List<String> cc){ this.cc = cc; }

    public void setBcc(List<String> bcc){ this.bcc = bcc; }

    public void setSubject(String subject){ this.subject = subject; }

    public void setBody(String body){ this.body = body; }

    public void setPriority(Priorities priority){ this.priority = priority; }

    public List<String> getTo(){ return to; }

    public List<String> getCc(){ return cc; }

    public List<String> getBcc(){ return bcc; }

    public String getSubject(){ return subject; }

    public String getBody(){ return body; }

    public Enum getPriority() { return priority; }
}
