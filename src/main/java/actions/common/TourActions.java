package actions.common;

import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import pages.tour.TourPage;
import pages.tour.impl.*;
import utils.gestures.PerformGesture;

public class TourActions {

    private TourPage tourPage;
    private Sut sut;

    public TourActions(TourPage tourPage) {
        this.sut = tourPage.getSut();
        this.tourPage = tourPage;
    }

    public MobileElement getPageHeading() {
        return tourPage.pageHeading;
    }

    public MobileElement getDescription() {
        return tourPage.pageDescription;
    }

    public MobileElement getButton() {
        return tourPage.pageButton;
    }

    public MobileElement getDontShowAgainCheckBox() {
        return tourPage.dontShowAgainCheckBox;
    }

    public void swipeToChangePage(AppiumDriver driver, boolean swipeLeft, MobileElement element) {
        if (swipeLeft) {
            PerformGesture.swipeToChangePage(driver, "left", element);
        } else {
            PerformGesture.swipeToChangePage(driver, "right", element);
        }
    }

    public TourPage swipeThroughAllTourPages(AppiumDriver driver) {
        TourPage tourPage = null;
        if (driver.getPlatformName().equalsIgnoreCase("Android")) {
            tourPage = swipeThroughAndroidTourPages(driver);
        } else if (driver.getPlatformName().equalsIgnoreCase("iOS")) {
            tourPage = swipeThroughIOSTourPages(driver);
        }
        return tourPage;
    }

    private TourPage swipeThroughAndroidTourPages(AppiumDriver driver) {
        for (int i = 0; i < 8; i++) {
            swipeToChangePage(driver, true, null);
        }
        return new TourPage8Impl(sut);
    }

    private TourPage swipeThroughIOSTourPages(AppiumDriver driver) {
        TourPage tp = new TourPage1Impl(sut);
        System.out.println("Page description = "+tp.pageDescription.getText());
        swipeToChangePage(driver, true, tp.pageDescription);
        tp = new TourPage2Impl(sut);
        swipeToChangePage(driver, true, tp.pageDescription);
        tp = new TourPage3Impl(sut);
        swipeToChangePage(driver, true, tp.pageDescription);
        tp = new TourPage4Impl(sut);
        swipeToChangePage(driver, true, tp.pageDescription);
        tp = new TourPage5Impl(sut);
        swipeToChangePage(driver, true, tp.pageDescription);
        tp = new TourPage6Impl(sut);
        swipeToChangePage(driver, true, tp.pageDescription);
        tp = new TourPage7Impl(sut);
        swipeToChangePage(driver, true, tp.pageDescription);
        tp = new TourPage8Impl(sut);
        swipeToChangePage(driver, true, tp.pageDescription);
        return tp;
    }

    public void clickDontShowAgain() {
        tourPage.dontShowAgainCheckBox.click();
    }

    public void clickButton() {
        tourPage.pageButton.click();
    }
}
