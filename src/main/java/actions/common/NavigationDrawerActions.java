package actions.common;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import pages.maillists.MailListPage;
import pages.maillists.impl.*;
import pages.navvigationdrawer.NavigationDrawerPage;
import pages.popups.Popup;
import pages.popups.permission.impl.NewFolderPopup;
import pages.profile.ProfilePage;
import pages.settings.SettingsPage;
import utils.gestures.TouchActionGestures;

public class NavigationDrawerActions {

    public NavigationDrawerPage navigationDrawerPage;
    public Sut sut;

    public NavigationDrawerActions(Sut sut) {
        this.sut = sut;
        navigationDrawerPage = new NavigationDrawerPage(this.sut);
    }

    public enum NavigationDrawerItem {
        DRAFT, TRASH, PROFILE, SETTINGS, OUTBOX, SENT, INBOX;
    }

    public Page openNavigationDrawerItem(NavigationDrawerItem folder) {
        Page page = null;
        switch (folder) {
            case DRAFT:
                navigationDrawerPage.draftsFolder.click();
                page = new MailListPageDraft(sut);
                break;
            case TRASH:
                navigationDrawerPage.trashFolder.click();
                page = new MailListPageTrash(sut);
                break;
            case PROFILE:
                navigationDrawerPage.profileBtn.click();
                page = new ProfilePage(sut);
                break;
            case SENT:
                navigationDrawerPage.sentFolder.click();
                page = new MailListPageSent(sut);
                break;
            case INBOX:
                navigationDrawerPage.inboxFolder.click();
                page = new MailListPageInbox(sut);
                break;
            case OUTBOX:
                navigationDrawerPage.outboxFolder.click();
                page = new MailListPageOutbox(sut);
                break;
            case SETTINGS:
                navigationDrawerPage.settingsBtn.click();
                page = new SettingsPage(sut);
                break;
        }
        return page;
    }

    public void closeNavigationDrawer() {
        Dimension d = sut.getAppiumDriver().manage().window().getSize();
        int x = d.width - 2;
        int y = (d.height) / 2;
        TouchActionGestures.tapOnPOINT(sut.getAppiumDriver(), new Point(x, y));
    }

    public void createFolder(String folderName) {
        navigationDrawerPage.createFolderBtn.click();
            NewFolderPopup popup = new NewFolderPopup(sut);
            popup.enterFolderNameField.sendKeys(folderName);
            popup.clickButton2();

    }

   /* public MobileElement getFolder(String folderName) {
        MobileElement folder = null;
        try {
            if (sut.getPlatform().equals(Sut.Platform.ANDROID)) {
                String scrollAreaID = "com.synchronoss.mail.qa:id/rec_nav_view";
                TouchActionsHelper.scrollToElement(driver, scrollAreaID, null, folderName);
                folder = (MobileElement) driver.findElementByXPath("//android.widget.TextView[@text='" + folderName + "']");
            } else {
                folder = foldersSection.findElementByAccessibilityId(folderName);
            }
        } catch (NoSuchElementException e) {
            System.out.println("No such folder found.");
        }

        return folder;
    }

    public void renameFolder(String folderName, String newName) {
        if (sut.getPlatform().equals(Sut.Platform.IOS)) {
            MobileElement folder = (MobileElement) ((IOSDriver) sut.getAppiumDriver()).findElementByIosNsPredicate("type == 'XCUIElementTypeCell' AND label BEGINSWITH[c] 'folder name " + folderName + "'");
            Point p = folder.findElementById("menu-right-arrow").getCenter();
            TouchActionsHelper.iOSSwipe(driver, "left", folder);
            TouchActionsHelper.tapOnAnElement(driver, p);
            IOSPopupPage popupPage = new IOSPopupPage(driver);

            MobileElement popupWindow = (MobileElement) driver.findElement(By.id("Rename Folder: " + folderName));
            MobileElement folderNameField = popupWindow.findElementByClassName("XCUIElementTypeTextField");
            folderNameField.clear();
            driver.findElementById("New folder name").sendKeys(newName);
            popupPage.renameFolderBtn.click();
        } else {
            getFolder(folderName).click();
            AndroidToolbar toolbar = new AndroidToolbar(driver);
            AndroidPopuppage popuppage = new AndroidPopuppage(driver);
            toolbar.moreOptionsBtn.click();
            toolbar.moreOptions_rename.click();
            popuppage.enterFolderNameField.clear();
            popuppage.enterFolderNameField.sendKeys(newName);
            popuppage.createFolderBtn.click();
        }
    }

    public void deleteAFolder(String folderName) {
        if (isiOS()) {
            MobileElement folder = getFolder(folderName);
            Point p = folder.getCenter();
            System.out.println(p.getX() + ", " + p.getY());
            TouchActionsHelper.iOSSwipe(driver, "left", folder);
            //TouchActionsHelper.tapOnAnElement(driver,folder.getCenter());
            ((IOSDriver) driver).findElementByIosNsPredicate("label == 'MoreButton'").click();
            ioS_deleteFolderOption.click();
            (new IOSPopupPage(driver)).okBrn.click();
        } else {
            getFolder(folderName).click();
            AndroidToolbar toolbar = new AndroidToolbar(driver);
            toolbar.moreOptionsBtn.click();
            toolbar.moreOptions_delete.click();
            WaitHelper.sleep(2000);
            (new AndroidPopuppage(driver)).actionBtn.click();
        }
    }

    public void createSubfolder(String folderName, String subFolderName) {
        if (sut.getPlatform().equals(Sut.Platform.IOS)) {
            MobileElement folder = getFolder(folderName);
            TouchActionsHelper.iOSSwipe(driver, "left", folder);
            ((IOSDriver) driver).findElementByIosNsPredicate("label CONTAINS 'subfolderButton'").click();
            IOSPopupPage popupPage = new IOSPopupPage(driver);
            popupPage.enterFolderNameField.sendKeys(subFolderName);
            popupPage.createFolderBtn.click();
        } else {
            getFolder(folderName).click();
            WaitHelper.sleep(3000);
            AndroidToolbar toolbar = new AndroidToolbar(driver);
            toolbar.moreOptionsBtn.click();
            toolbar.moreOptions_newFolder.click();
            AndroidPopuppage popupPage = new AndroidPopuppage(driver);
            popupPage.enterFolderNameField.sendKeys(subFolderName);
            popupPage.createFolderBtn.click();
        }
    }*/
}
