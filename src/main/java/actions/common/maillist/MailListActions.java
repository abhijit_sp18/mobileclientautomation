package actions.common.maillist;

import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import pages.maillists.MailListPage;
import pages.navvigationdrawer.NavigationDrawerPage;
import utils.gestures.IOSGestures;
import utils.gestures.TouchActionGestures;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class MailListActions {

    MailListPage mailListPage;
    Sut sut;

    public MailListActions(MailListPage mailListPage) {
        this.mailListPage = mailListPage;
        this.sut = mailListPage.getSut();
    }

    public NavigationDrawerPage openNavigationDrawer(){
        mailListPage.navigationDrawerBtn.click();
        return new NavigationDrawerPage(sut);
    }

    public MobileElement findEmailBySubject(String subject) {
        MobileElement email = null;
        List<MobileElement> emails = mailListPage.emailContainers;
        for (MobileElement currentEmail : emails) {
            String currentSubject = getSubjectInEmail(currentEmail).getText();
            if (currentSubject.equals(subject)) {
                email = currentEmail;
                break;
            }
        }
        return email;
    }

    public MobileElement getSubjectInEmail(MobileElement emailContainer) {
        MobileElement subject = null;
        if (sut.getPlatform().equals(Sut.Platform.ANDROID)) {
            subject = emailContainer.findElement(mailListPage.androidSubjectBy);
        } else if (sut.getPlatform().equals(Sut.Platform.IOS)) {
            subject = emailContainer.findElement(mailListPage.iOSSubjectBy);
        }
        return subject;
    }

    public void swipeRightOnEmail(MobileElement emailContainer) {
        MobileElement leftAndroidElement = emailContainer.findElement(mailListPage.androidAvatarBy);
        MobileElement rightAndroidElement = emailContainer.findElement(mailListPage.androidTimeStampBy);
        Duration d = Duration.ofMillis(200);
        if (sut.getAppiumDriver().getPlatformName().equalsIgnoreCase("Android")) {
            TouchActionGestures.swipeFromElementToElement(sut.getAppiumDriver(), leftAndroidElement, rightAndroidElement, d, d);
        } else if (sut.getAppiumDriver().getPlatformName().equalsIgnoreCase("iOS")) {
            IOSGestures.swipe(sut.getAppiumDriver(), "right", emailContainer);
        }
    }

    public void swipeLeftOnEmail(MobileElement emailContainer) {
        MobileElement leftAndroidElement = emailContainer.findElement(mailListPage.androidAvatarBy);
        MobileElement rightAndroidElement = emailContainer.findElement(mailListPage.androidTimeStampBy);
        Duration d = Duration.ofMillis(200);
        if (sut.getAppiumDriver().getPlatformName().equalsIgnoreCase("Android")) {
            TouchActionGestures.swipeFromElementToElement(sut.getAppiumDriver(), rightAndroidElement, leftAndroidElement, d, d);
        } else if (sut.getAppiumDriver().getPlatformName().equalsIgnoreCase("iOS")) {
            IOSGestures.swipe(sut.getAppiumDriver(), "left", emailContainer);
        }
    }

    public void selectEmailsBySubject(List<String> subjects) {
        boolean isAndroid = sut.getAppiumDriver().getPlatformName().equalsIgnoreCase("Android");
        for (int i = 0; i < subjects.size(); i++) {
            if (i == 0) {
                MobileElement firstSubject = findEmailBySubject(subjects.get(i));
                if (isAndroid) {
                    TouchActionGestures.longPressOnElement(sut.getAppiumDriver(), firstSubject, 2.0);
                } else {
                    mailListPage.iOSEditBtn.click();
                    firstSubject.click();
                }
            } else {
                findEmailBySubject(subjects.get(i)).click();
            }
        }
    }

    public void selectEmailBySubject(String subject) {
        List<String> list = new ArrayList<>();
        list.add(subject);
        selectEmailsBySubject(list);
    }
}
