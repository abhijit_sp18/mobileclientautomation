package actions.common;

import com.synchronoss.cqe.common.mobile.core.Sut;
import pages.login.LoginPage;

public class LoginActions {

    private LoginPage loginPage;
    private Sut sut;

    public LoginActions(Sut sut){
        this.loginPage=new LoginPage(sut);
        this.sut = sut;
    }

    public void login(String userName, String password){
        loginPage.usernameField.sendKeys(userName);
        loginPage.passwordField.sendKeys(password);
        loginPage.loginButton.click();
    }

}
