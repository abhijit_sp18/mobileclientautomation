package actions.common;

import com.synchronoss.cqe.common.mobile.core.Sut;
import pages.maildetails.MailDetailsPage;

public class MailDetailsActions {

    private MailDetailsPage mailDetailsPage;
    private Sut sut;

    public MailDetailsActions(Sut sut){
        this.sut = sut;
        mailDetailsPage = new MailDetailsPage(sut);
    }

    public void openMoreOptions(){
            mailDetailsPage.optionsContainer.click();
    }

    public void markEmailAsFlagged(){
        openMoreOptions();
        if (sut.getPlatform().equals(Sut.Platform.ANDROID)){
            mailDetailsPage.clickButton(mailDetailsPage.androidMoreOptions.flag);
        } else if (sut.getPlatform().equals(Sut.Platform.IOS)){
            mailDetailsPage.clickButton(mailDetailsPage.iosMoreOptions.flag);
        }
        else {
            closeOptions();
        }
    }

    public void markEmailAsUnFlagged(){
        openMoreOptions();
        if (sut.getPlatform().equals(Sut.Platform.ANDROID)){
            mailDetailsPage.clickButton(mailDetailsPage.androidMoreOptions.unFlag);
        } else if (sut.getPlatform().equals(Sut.Platform.IOS)){
            mailDetailsPage.clickButton(mailDetailsPage.iosMoreOptions.unFlag);
        }
        else {
            closeOptions();
        }
    }

    public void markEmailAsRead(){
        openMoreOptions();
        if (sut.getPlatform().equals(Sut.Platform.ANDROID)){
            mailDetailsPage.clickButton(mailDetailsPage.androidMoreOptions.read);
        } else if (sut.getPlatform().equals(Sut.Platform.IOS)){
            mailDetailsPage.clickButton(mailDetailsPage.iosMoreOptions.read);
        }
        else {
            closeOptions();
        }
    }

    public void markEmailAsUnRead(){
        openMoreOptions();
        if (sut.getPlatform().equals(Sut.Platform.ANDROID)){
            mailDetailsPage.clickButton(mailDetailsPage.androidMoreOptions.unread);
        } else if (sut.getPlatform().equals(Sut.Platform.IOS)){
            mailDetailsPage.clickButton(mailDetailsPage.iosMoreOptions.unread);
        }
        else {
            closeOptions();
        }
    }

    public void closeOptions(){
        if (sut.getPlatform().equals(Sut.Platform.ANDROID)){
            sut.navigateBack();
        } else {
            mailDetailsPage.iosMoreOptions.cancel.click();
        }
    }
}
