package actions.iosspecific;

import com.synchronoss.cqe.common.mobile.core.Sut;
import pages.iosspecific.SelectEnvironment;

public class SelectEnvironmentActions {

    private SelectEnvironment selectEnvironment;
    private Sut sut;

    public SelectEnvironmentActions(Sut sut){
        this.sut=sut;
        selectEnvironment = new SelectEnvironment(sut);
    }

    public void clickDoneBtn(){
        selectEnvironment.doneBtn.click();
    }
}
