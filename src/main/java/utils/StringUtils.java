package utils;

import java.security.SecureRandom;

public class StringUtils {

    public static String getRandomString(){
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        SecureRandom sr = new SecureRandom();
        StringBuilder sb = new StringBuilder(10);
        for (int i=0; i<10; i++){
            sb.append(alphabet.charAt(sr.nextInt(alphabet.length())));
        }
        return sb.toString();
    }

}
