package utils.gestures;

import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.JavascriptExecutor;

import java.util.HashMap;
import java.util.Map;

public class IOSGestures {

    public static void swipe(AppiumDriver driver, String direction, MobileElement element){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        Map<String, Object> params = new HashMap<>();
        params.put("direction", direction);
        params.put("element", element.getId());
        js.executeScript("mobile: swipe", params);
    }

    public static void touchAndHold(AppiumDriver driver, MobileElement element, Double duration){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        Map<String, Object> tfLongTap = new HashMap<>();
        tfLongTap.put("element", element.getId());
        tfLongTap.put("duration", duration);
        js.executeScript("mobile: touchAndHold", tfLongTap);

    }
}
