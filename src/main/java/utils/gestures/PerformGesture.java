package utils.gestures;

import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import utils.TouchActionsHelper;

import java.time.Duration;

public class PerformGesture {

    public static void swipeToChangePage(AppiumDriver driver, String direction, MobileElement element){
        if (driver.getPlatformName().equalsIgnoreCase("Android")) {
            TouchActionGestures.swipeToChangePage(driver, direction);
        } else if(driver.getPlatformName().equalsIgnoreCase("iOS")){
            IOSGestures.swipe(driver, direction, element);
        }
    }

    public static void longPressOnElement(AppiumDriver driver, MobileElement element, double duration){
    if (driver.getPlatformName().equalsIgnoreCase("Android")){
        TouchActionGestures.longPressOnElement(driver, element, duration);
    } else if(driver.getPlatformName().equalsIgnoreCase("iOS")){
        IOSGestures.touchAndHold(driver, element, duration);
    }
    }
}
