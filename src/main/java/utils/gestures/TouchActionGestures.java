package utils.gestures;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

import java.time.Duration;

public class TouchActionGestures {

    public static void swipeFromPointToPoint(AppiumDriver driver, Point start, Point end, Duration startDuration, Duration endDuration){
        TouchAction touchAction = new TouchAction(driver);
        WaitOptions startWait = WaitOptions.waitOptions(startDuration);
        WaitOptions endWait = WaitOptions.waitOptions(endDuration);
        touchAction.longPress(PointOption.point(start))
                .waitAction(startWait)
                .moveTo(PointOption.point(end))
                .waitAction(endWait)
                .perform();
    }

    public static void swipeToChangePage(AppiumDriver driver,String direction){
        Dimension screenDimensions = driver.manage().window().getSize();
        int width = screenDimensions.width;
        int height = screenDimensions.height;
        int heightMid = (int) (height/2);
        int screenEnd = (int) (0.99*width);
        Point leftEnd = new Point(0, heightMid);
        Point rightEnd = new Point(screenEnd, heightMid);
        Duration duration = Duration.ofMillis(1000);

    switch (direction){
        case "right" : swipeFromPointToPoint(driver, leftEnd, rightEnd,duration, duration);
        break;
        case "left" : swipeFromPointToPoint(driver, rightEnd, leftEnd,duration, duration);
        break;
    }
    }

    public static void swipeFromElementToElement(AppiumDriver driver, MobileElement start, MobileElement end
            , Duration startDuration, Duration endDuration){
      Point startPoint = start.getCenter();
      Point endPoint = end.getCenter();
      swipeFromPointToPoint(driver, startPoint, endPoint, startDuration, endDuration);
    }

    public static void longPressOnElement(AppiumDriver driver, MobileElement element, double duration){
        TouchAction touchAction = new TouchAction(driver);
        touchAction.longPress(LongPressOptions.longPressOptions()
                .withElement(ElementOption.element(element))
                .withDuration(Duration.ofSeconds((long)duration)));
    }

    public static void tapOnPOINT(AppiumDriver driver, Point point){
        int x = point.getX();
        int y= point.getY();
        (new TouchAction<>(driver)).tap(PointOption.point(x, y)).perform();
    }
}
