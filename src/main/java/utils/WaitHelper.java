package utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitHelper {

    public static void sleep(long millis){
        try{
            Thread.sleep(millis);
        } catch (InterruptedException e){
            e.printStackTrace();
        }

    }

    public static void waitUntilPresenceOfElement(AppiumDriver driver, long millis, MobileElement element){
        WebDriverWait wait = new WebDriverWait(driver, millis);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitUntilPresenceOfAllElements(AppiumDriver driver, long millis, By by){
        WebDriverWait wait = new WebDriverWait(driver, millis);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
    }

    public static void waitUntilInvisibilityOfElement(AppiumDriver driver, long millis, MobileElement element){
        WebDriverWait wait = new WebDriverWait(driver, millis);
        wait.until(ExpectedConditions.invisibilityOf(element));
    }

    public static void waitWithCustomExpectedConditions(AppiumDriver driver, long seconds, ExpectedCondition<Boolean> customCondition){
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(customCondition);
    }
}
