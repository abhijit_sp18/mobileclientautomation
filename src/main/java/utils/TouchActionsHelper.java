package utils;

import com.google.common.collect.ImmutableMap;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.RemoteWebElement;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

public class TouchActionsHelper {
    public static void longPressOnAnElement(AppiumDriver driver, MobileElement element){
        TouchAction t = new TouchAction(driver);
        t.longPress(LongPressOptions.longPressOptions()
                .withElement(ElementOption.element(element)))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(3000))).perform();
    }

    public static void longPressOnAnElement(AppiumDriver driver, Point point){
        TouchAction t = new TouchAction(driver);
        t.longPress(PointOption.point(point.getX()+10, point.getY()))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(3000))).perform();
    }

    public static void longPress(AppiumDriver driver, MobileElement element){
        TouchAction action;
        if (driver.getPlatformName().equalsIgnoreCase("iOS")){
            action = new TouchAction((IOSDriver) driver);
        } else {
            action = new TouchAction((AndroidDriver) driver);
        }
        action.press(PointOption.point(element.getCenter()));
        action.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)));
        action.release();
        action.perform();
    }

    public static void tapOnAnElement(AppiumDriver driver, Point point){
        int x = point.getX();
        int y= point.getY();
        (new TouchAction<>(driver)).tap(PointOption.point(x, y)).perform();
    }

    public static void tapOnAnElement(AppiumDriver driver, int x, int y){
        (new TouchAction<>(driver)).tap(PointOption.point(x, y)).perform();
        System.out.println("Tapped on "+ x + ", " +y);
    }

    public static void swipeLeft(AppiumDriver driver, MobileElement fromElement, MobileElement toElement){
        TouchAction t = new TouchAction(driver);
        PointOption p1 = PointOption.point(fromElement.getCenter().getX(),fromElement.getCenter().getY());
        PointOption p2 = PointOption.point(toElement.getCenter().getX(),toElement.getCenter().getY());
        WaitOptions w = WaitOptions.waitOptions(Duration.ofMillis(1000));
        t.press(p1).waitAction(w).moveTo(p2).waitAction(w).perform();
    }

    public static void swipeUpFromScreenCentre(AppiumDriver driver, int numberOfTimes){
        TouchAction t = new TouchAction(driver);
        Dimension screenDimens = driver.manage().window().getSize();
        int centreX = (screenDimens.width)/2;
        int centreY = (screenDimens.height)/2;
        int topx = centreX;
        int topY = screenDimens.height - ((int) (0.95*screenDimens.height));
        PointOption p1= PointOption.point(centreX, centreY);
        PointOption p2= PointOption.point(topx, topY);
        WaitOptions w = WaitOptions.waitOptions(Duration.ofMillis(500));
        while (numberOfTimes-- > 0) {
            t.press(p1).waitAction(w).moveTo(p2).waitAction(w).release().perform();
        }
    }

    public static void swipeDownFromScreenCentre(AppiumDriver driver, int numberOfTimes){
        TouchAction t = new TouchAction(driver);
        Dimension screenDimens = driver.manage().window().getSize();
        int centreX = (screenDimens.width)/2;
        int centreY = (int) ((screenDimens.height)* 0.3);
        int topx = centreX;
        int bottomYold = screenDimens.height - ((int) (0.001*(screenDimens.height)));
        int bottomY = screenDimens.height;
        System.out.println(topx + ", "+ bottomY +"      bottomYOld = " + bottomYold);
        System.out.println(centreX + ", "+ centreY );
        PointOption p1= PointOption.point(centreX, centreY);
        PointOption p2= PointOption.point(topx, bottomY);
        WaitOptions w = WaitOptions.waitOptions(Duration.ofMillis(200));
        WaitOptions w1 = WaitOptions.waitOptions(Duration.ofMillis(1500));
        while (numberOfTimes-- > 0) {
            t.press(p1).waitAction(w).moveTo(p2).waitAction(w1).release().perform();
        }
    }

    public static void swipeUpFromElement(AppiumDriver driver, MobileElement element, int numberOfTimes){
        TouchAction t = new TouchAction(driver);
        Dimension screenDimens = driver.manage().window().getSize();
        int centreX = (screenDimens.width)/2;
        int topY = screenDimens.height - ((int) (0.95*screenDimens.height));
        int elementX = element.getCenter().getX();
        int elementY = element.getCenter().getY();
        PointOption p1= PointOption.point(elementX, elementY);
        PointOption p2= PointOption.point(centreX, topY);
        WaitOptions w = WaitOptions.waitOptions(Duration.ofMillis(500));
        while (numberOfTimes-- > 0) {
            t.press(p1).waitAction(w).moveTo(p2).waitAction(w).perform();
        }
    }

    public static void swipeFromElementToElement(AppiumDriver driver, MobileElement start, MobileElement end){
        TouchAction t = new TouchAction(driver);
        PointOption p1= PointOption.point(start.getCenter().getX(), start.getCenter().getY());
        PointOption p2= PointOption.point(end.getCenter().getX(), end.getCenter().getY());
        WaitOptions w = WaitOptions.waitOptions(Duration.ofMillis(100));

        t.press(p1).waitAction(w).moveTo(p2).waitAction(w).perform();

    }

    public static void swipeFromPointToPoint(AppiumDriver driver, Point start, Point end){
        TouchAction t = new TouchAction(driver);

        PointOption p1= PointOption.point(start);
        PointOption p2= PointOption.point(end);
        WaitOptions w = WaitOptions.waitOptions(Duration.ofMillis(300));

        t.press(p1).waitAction(w).moveTo(p2).waitAction(w).perform();
        System.out.println("Swiped from " + start.toString() + " to " + end.toString());
    }


    public static void iOSSwipe(AppiumDriver driver, String swipeDir, MobileElement element){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        Map<String, Object> params = new HashMap<>();
        params.put("direction", swipeDir);
        params.put("element", element.getId());
        js.executeScript("mobile: swipe", params);
    }

    public static void androidSwipe(AppiumDriver driver, MobileElement startElement, MobileElement endElement){
        Actions a = new Actions(driver);
        a.dragAndDrop(startElement, endElement).perform();
    }

    public static void scrollToElement(AppiumDriver driver, String androidScrollAreaID, By iOSScrollAreaBy, String elementID){
        if (driver.getPlatformName().equalsIgnoreCase("Android")) {
            androidScroll(driver, androidScrollAreaID, elementID);
        } else {
            iOSScroll(driver, iOSScrollAreaBy, elementID);
        }
    }

    public static void androidScroll(AppiumDriver driver, String scrollAreaID, String elementID){
   /*     ((AndroidDriver) driver).findElementByAndroidUIAutomator(
                "new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+elementID+"\").instance(0))");
   */
        System.out.println("Android Scroll start");
        ((AndroidDriver) driver).findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()"
                + ".resourceId(\""+ scrollAreaID+"\")).scrollIntoView("
                + "new UiSelector().textContains(\""+elementID+"\"));");
        System.out.println("Android Scroll end");
    }
    public static void iOSScroll(AppiumDriver driver, By scrollAreaBy, String elementID){
        RemoteWebElement e = (RemoteWebElement) driver.findElement(scrollAreaBy);
        String parentID = e.getId();
        HashMap<String, String> scrollObject = new HashMap<>();
        scrollObject.put("element", parentID);
        scrollObject.put("name", elementID);
        //scrollObject.put("simpleIsVisibleCheck", "true");
        driver.executeScript("mobile:scroll", scrollObject);

    }

    public static void iOSScrollUsingPredicateString(AppiumDriver driver, By scrollAreaBy, String predicateString){
        RemoteWebElement e = (RemoteWebElement) driver.findElement(scrollAreaBy);
        String parentID = e.getId();
        HashMap<String, String> scrollObject = new HashMap<>();
        scrollObject.put("element", parentID);
        scrollObject.put("predicateString", predicateString);
        //scrollObject.put("simpleIsVisibleCheck", "true");
        driver.executeScript("mobile:scroll", scrollObject);
    }

    public static void openNotifications(AppiumDriver driver){
        if (driver.getPlatformName().equalsIgnoreCase("Android")){
            ((AndroidDriver) driver).openNotifications();
        } else{
            Dimension d = driver.manage().window().getSize();
            int centre = (d.width)/2;
            int maxHeight = d.height;
            Point p1 =  new Point(centre, 0);
            Point p2 = new Point(centre, maxHeight);
            swipeFromPointToPoint(driver, p1, p2);
        }
    }

    public static void openControlCentre(AppiumDriver driver){
   /*     Dimension d = driver.manage().window().getSize();
        int edgeOfScreen = (int) ((d.width)* 0.87);
        int maxHeight = (int) ((d.height)* 0.8);
        int maxWidth = d.width;
        Point p1 =  new Point(edgeOfScreen, 2);
        Point p2 = new Point(edgeOfScreen, maxHeight);

        TouchAction t = new TouchAction(driver);

        PointOption po1= PointOption.point(p1);
        PointOption po2= PointOption.point(p2);
        WaitOptions w = WaitOptions.waitOptions(Duration.ofMillis(300));

        t.press(po1).waitAction(w).moveTo(po2).waitAction(w).perform();
        System.out.println("Swiped from " + p1.toString() + " to " + p2.toString());*/
        //swipeFromPointToPoint(driver, p1, p2);

        MobileElement e = (MobileElement) ((IOSDriver) driver).findElementByIosNsPredicate("type == 'XCUIElementTypeOther' AND name CONTAINS 'Wi-Fi bars'");
        iOSSwipe(driver, "down",e);
    }

    public static void iOSCloseNotifications(AppiumDriver driver){
        Dimension d = driver.manage().window().getSize();
        int centre = (d.width)/2;
        int maxHeight = d.height;
        Point p1 =  new Point(centre, 0);
        Point p2 = new Point(centre, maxHeight);
        swipeFromPointToPoint(driver, p2, p1);
    }

    public static void androidCloseNotifications(AppiumDriver driver){
        Dimension d = driver.manage().window().getSize();
        int centre = (d.width)/2;
        int maxHeight = (d.height)/2;
        Point p1 =  new Point(centre, 0);
        Point p2 = new Point(centre, maxHeight);
        swipeFromPointToPoint(driver, p2, p1);
    }

    public static void closeNotifications(AppiumDriver driver){
        if (driver.getPlatformName().equalsIgnoreCase("Android")){
            //androidCloseNotifications(driver);
            driver.launchApp();
        } else {
            iOSCloseNotifications(driver);
        }
    }

    public static void iosSwipeDown(AppiumDriver driver){
        Map<String, Object> args = new HashMap<>();
        args.put("direction", "down");
        driver.executeScript("mobile: swipe", args);
    }

    public static void pullToRefresh(AppiumDriver driver){
        swipeDownFromScreenCentre(driver, 1);
        if (driver.getPlatformName().equalsIgnoreCase("Android")){
            try {
                driver.findElementById("selected_icon").click();
            } catch (NoSuchElementException e){
                System.out.println("No mail was selected while performing pull to refresh.");
            }

        }
    }

    public static void tapOnCentreOfTheScreen(AppiumDriver driver){
        TouchAction t = new TouchAction(driver);

        Dimension d = driver.manage().window().getSize();
        int cx = (d.width)/2;
        int cy = (d.height)/2;
        t.tap(PointOption.point(cx, cy)).perform();
    }

    public static void pressHomeButton(AppiumDriver driver){
        if (driver.getPlatformName().equalsIgnoreCase("iOS")){
            driver.executeScript("mobile: pressButton", ImmutableMap.of("name", "home"));
        } else {
            ((AndroidDriver)driver).pressKey(new KeyEvent(AndroidKey.HOME));
        }
    }
}

