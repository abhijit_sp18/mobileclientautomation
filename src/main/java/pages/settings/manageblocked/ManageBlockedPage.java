package pages.settings.manageblocked;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

import java.time.temporal.ChronoUnit;

public class ManageBlockedPage extends Page {

    public ManageBlockedPage(Sut sut) {
        super(sut);
    }

    @AndroidFindBy(id = "addEmailButton")
    @iOSXCUITFindBy(id = "AddOrCancel")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement manageBlocked_addBtn;

    @iOSXCUITFindBy(accessibility = "Add new email address")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement iOS_manageBlocked_addEmailOption;

    @AndroidFindBy(id = "input")
    @iOSXCUITFindBy(id = "Enter sender email")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement manageBlocked_enterEmailField;

    @AndroidFindBy(id = "dialog_positive_button")
    @iOSXCUITFindBy(accessibility = "OK")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement manageBlocked_confirmBtn;

    @AndroidFindBy(id = "common_dialog_action_button")
    @iOSXCUITFindBy(accessibility = "OK")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement manageBlocked_deleteConfirmBtn;
}
