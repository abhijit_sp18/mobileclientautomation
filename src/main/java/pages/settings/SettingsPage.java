package pages.settings;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.*;
import org.openqa.selenium.NoSuchElementException;

import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SettingsPage extends Page {

    public SettingsPage(Sut sut) {
        super(sut);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='About']")
    @iOSXCUITFindBy(accessibility = "About")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement about;

    @AndroidFindBy(xpath = "xpath")
    @iOSXCUITFindBy(accessibility = "Contacts Source")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement contactsSource;

    @AndroidFindBy(xpath = "xpath")
    @iOSXCUITFindBy(id = "Manage Accounts")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement manageAccount;

    @AndroidFindBy(xpath = "(//android.widget.CheckBox[@resource-id='android:id/checkbox'])[1]")
    @iOSXCUITFindBy(iOSNsPredicate = "type == 'XCUIElementTypeSwitch' AND label == 'Remember Recipient'")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement rememberRecipientSwitch;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Remember recipient']")
    @iOSXCUITFindBy(iOSNsPredicate = "type == 'XCUIElementTypeStaticText' AND value == 'Remember Recipient'")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement rememberRecipientSettingLabel;

    @AndroidFindBy(xpath = "(//android.widget.CheckBox[@resource-id='android:id/checkbox'])[2]")
    @iOSXCUITFindBy(iOSNsPredicate = "type == 'XCUIElementTypeSwitch' AND label == 'Show Categories'")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement showCategoriesSwitch;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Show categories']")
    @iOSXCUITFindBy(iOSNsPredicate = "type == 'XCUIElementTypeStaticText' AND value == 'Show Categories'")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement showCategoriesSettingLabel;

    @AndroidFindBy(xpath = "(//android.widget.CheckBox[@resource-id='android:id/checkbox'])[3]")
    @iOSXCUITFindBy(iOSNsPredicate = "type == 'XCUIElementTypeSwitch' AND label == 'Conversation View'")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement conversationViewSwitch;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Show categories']")
    @iOSXCUITFindBy(iOSNsPredicate = "type == 'XCUIElementTypeStaticText' AND value == 'Conversation View'")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement conversationViewSettingLabel;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Signature']")
    @iOSXCUITFindBy(id = "Signature")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement signature;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Out-of-office message']")
    @iOSXCUITFindBy(id = "Out-of-office message")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement oooMessage;

    @AndroidFindBy(xpath = "(//android.widget.CheckBox[@resource-id='android:id/checkbox'])[4]")
    @iOSXCUITFindBy(iOSNsPredicate = "type == 'XCUIElementTypeSwitch' AND label == 'Show Avatar'")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement showAvatarSwitch;

    @AndroidFindBy(uiAutomator = "new UiScrollable(new UiSelector().resourceId(\"com.synchronoss.mail.qa:id/recycler_view\")).scrollIntoView(new UiSelector().textContains(\"Block all images\"));")
    @iOSXCUITFindBy(id = "Block Images")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement blockImages;

    @AndroidFindBy(uiAutomator = "new UiScrollable(new UiSelector().resourceId(\"com.synchronoss.mail.qa:id/recycler_view\")).scrollIntoView(new UiSelector().textContains(\"Reply quoting message\"));")
    @iOSXCUITFindBy(iOSNsPredicate = "type == 'XCUIElementTypeSwitch' AND label == 'Reply Quoting Message'")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement replyQuotingMessage;

    @iOSXCUITFindBy(accessibility = "Settings")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement backToSettingsBtn; // Add Android equivalent btn

    @AndroidFindBy(id = "signature")
    @iOSXCUITFindBys({
            @iOSXCUITBy(id = "SignatureView"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther)[3]")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement signature_signatureField;

    @AndroidFindBy(accessibility = "Open navigation drawer")
    @iOSXCUITFindBy(accessibility = "Done")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement closeSettingsBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Manage blocked']")
    @iOSXCUITFindBy(id = "Manage Blocked")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement manageBlocked;

    @iOSXCUITFindBy(accessibility = "Done Button")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement iOS_manageAllowed_doneBtn;

}
