package pages.settings.contactsources;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

import java.time.temporal.ChronoUnit;

public class ContactSourcesPage extends Page {

    public ContactSourcesPage(Sut sut) {
        super(sut);
    }

    @AndroidFindBy(xpath = "xpath")
    @iOSXCUITFindBy(accessibility = "Phone AddressBook")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement contactsSource_phoneAddressBook;

    @AndroidFindBy(xpath = "xpath")
    @iOSXCUITFindBy(accessibility = "Server AddressBook")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement contactsSource_serverAddressBook;

}
