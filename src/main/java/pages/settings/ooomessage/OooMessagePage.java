package pages.settings.ooomessage;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class OooMessagePage extends Page {

    public OooMessagePage(Sut sut) {
        super(sut);
    }

    @AndroidFindBys({
            @AndroidBy(id = "container"),
            @AndroidBy(xpath = "(//android.widget.RelativeLayout)[1]"),
            @AndroidBy(className = "android.widget.Switch")
    })
    @iOSXCUITFindBy(id = "vacationMessageSwitch")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement oooMessage_enable;

    @AndroidFindBys({
            @AndroidBy(id = "container"),
            @AndroidBy(xpath = "(//android.widget.RelativeLayout)[2]"),
            @AndroidBy(className = "android.widget.Switch")
    })
    @iOSXCUITFindBy(iOSNsPredicate = "type == 'XCUIElementTypeSwitch' AND name == 'vacationMessageSwitch' AND label == 'Quote original mail'")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement oooMessage_quoteOriginalMail;

    @iOSXCUITFindAll({
            @iOSXCUITBy(id = "dateButton")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public List<MobileElement> ios_oooMessage_dateFields;

    @AndroidFindBy(id = "from_date")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement android_oooMessage_fromDateField;

    @AndroidFindBy(id = "to_date")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement android_oooMessage_toDateField;

    @AndroidFindBy(id = "from_time")
    @iOSXCUITFindBy(id = "messageLabel")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement android_oooMessage_fromTimeField;

    @AndroidFindBy(id = "to_time")
    @iOSXCUITFindBy(id = "messageLabel")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement android_oooMessage_toTimeField;

    @AndroidFindBy(id = "message")
    @iOSXCUITFindBy(id = "messageLabel")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement customMessage;

    @AndroidFindBy(accessibility = "Open navigation drawer")
    @iOSXCUITFindBy(iOSNsPredicate = "type == 'XCUIElementTypeButton' AND label == 'Cancel'")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement oooMessage_cancelBtn;
}
