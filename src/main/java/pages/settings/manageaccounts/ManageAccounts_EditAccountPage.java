package pages.settings.manageaccounts;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

import java.time.temporal.ChronoUnit;

public class ManageAccounts_EditAccountPage extends Page {

    public ManageAccounts_EditAccountPage(Sut sut) {
        super(sut);
    }

    @iOSXCUITFindBy(accessibility = "Delete Account")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement deleteAccountBtn;

    @iOSXCUITFindBy(accessibility = "Delete")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement manageAccounts_deleteAccountConfirmationBtn;


    @iOSXCUITFindBy(accessibility = "Done")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement doneBtn;

    @iOSXCUITFindBy(accessibility = "IMAP")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement IMAPBtn;

    @iOSXCUITFindBy(accessibility = "POP")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement POPBtn;

    @iOSXCUITFindBy(id = "Incoming server address")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement manageAccounts_advanceSettingsBtn_incomingServerAddress;

    @iOSXCUITFindBy(accessibility = "OK")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement validationMessage_okBtn;

    @iOSXCUITFindBy(accessibility = "Please set a valid description")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement validationMessage_noDescription;

    @iOSXCUITFindBy(accessibility = "Email address cannot be empty")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement validationMessage_noEmailAddress;

    @iOSXCUITFindBy(accessibility = "Please provide a valid email address")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement validationMessage_invalidEmailAddress;

    @iOSXCUITFindBy(accessibility = "Password field cannot be empty")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement validationMessage_noPassword;

    @iOSXCUITFindBy(accessibility = "back icon")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement backButton;
}
