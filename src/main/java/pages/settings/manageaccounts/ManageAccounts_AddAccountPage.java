package pages.settings.manageaccounts;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSXCUITBy;
import io.appium.java_client.pagefactory.iOSXCUITFindAll;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class ManageAccounts_AddAccountPage extends Page {

    public ManageAccounts_AddAccountPage(Sut sut) {
        super(sut);
    }

    @iOSXCUITFindBy(id = "Description")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement description;

    @iOSXCUITFindBy(id = "Display Name")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement displayName;

    @iOSXCUITFindBy(id = "Email Address")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement mailAddress;

    @iOSXCUITFindBy(id = "Password")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement password;

    @iOSXCUITFindBy(accessibility = "Save")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement oneBtn;

    @iOSXCUITFindBy(accessibility = "Advanced Settings")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement advanceSettingsBtn;

    @iOSXCUITFindAll({
            @iOSXCUITBy(id = "Port")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public List<MobileElement> manageAccounts_addAccount_ports;

    @iOSXCUITFindBy(id = "Outgoing server address")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement manageAccounts_addAccount_outgoingServerAddress;
}
