package pages.settings.manageaccounts;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

import java.time.temporal.ChronoUnit;

public class ManageAccountsPage extends Page {

    public ManageAccountsPage(Sut sut) {
        super(sut);
    }

    @iOSXCUITFindBy(accessibility = "Add Account")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement manageAccounts_addAccount;
}
