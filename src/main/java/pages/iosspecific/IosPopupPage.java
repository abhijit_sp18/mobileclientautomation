package pages.iosspecific;


import base.PageBase;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import java.time.temporal.ChronoUnit;

public class IosPopupPage extends PageBase {

    public IosPopupPage(Sut sut){
        super(sut);
    }

    @HowToUseLocators(androidAutomation = LocatorGroupStrategy.ALL_POSSIBLE,
            iOSXCUITAutomation = LocatorGroupStrategy.ALL_POSSIBLE)

    @iOSXCUITFindBy (accessibility = "Delete Draft")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement deleteDraftBtn;

    @iOSXCUITFindBy (accessibility = "Delete Changes")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement deleteChangesBtn;

    @iOSXCUITFindBy (accessibility = "Save Draft")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement saveDraftBtn;

    @iOSXCUITFindBy (accessibility = "OK")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement okBrn;

    @iOSXCUITFindBy (accessibility = "Allow")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement allowBtn;

    @iOSXCUITFindBy (id = "Folder name")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement enterFolderNameField;

    @iOSXCUITFindBy (accessibility = "Create")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement createFolderBtn;

    @iOSXCUITFindBy (accessibility = "Rename")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement renameFolderBtn;

    @iOSXCUITFindBy (accessibility = "Cancel")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement cancelFolderCreationBtn;

    @iOSXCUITFindBy (accessibility = "Save Draft")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement saveAsDraft;

    @iOSXCUITFindBy (accessibility = "Invalid credentials, please provide correct one.")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement invalidCredentialsMessage;

    @iOSXCUITFindBy (accessibility = "Invalid Email")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement invalidUsernameMessage;

    @iOSXCUITFindBy (accessibility = "Notifications may include alerts, sounds, and icon badges. These can be configured in Settings.")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement notificationPermissionMessage;
}