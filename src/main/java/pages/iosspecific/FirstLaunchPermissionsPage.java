package pages.iosspecific;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

import java.time.temporal.ChronoUnit;

public class FirstLaunchPermissionsPage extends Page {

    public FirstLaunchPermissionsPage(Sut sut){
        super(sut);
    }
    @iOSXCUITFindBy(accessibility = "Close")
    @WithTimeout(time = 20, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement permissionsDialogCloseBtn;

    @iOSXCUITFindBy(accessibility = "Enable Calendar")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement enableCalendarPermission;

    @iOSXCUITFindBy(accessibility = "Enable PhotoLibrary")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement photoLibraryPermission;

    @iOSXCUITFindBy(accessibility = "Enable Contacts")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement enableContactsPermission;
}
