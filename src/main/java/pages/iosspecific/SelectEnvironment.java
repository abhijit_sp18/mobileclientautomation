package pages.iosspecific;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.HowToUseLocators;
import io.appium.java_client.pagefactory.LocatorGroupStrategy;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

import java.time.temporal.ChronoUnit;

public class SelectEnvironment extends Page {

    public SelectEnvironment(Sut sut){
        super(sut);
    }

  /*  @HowToUseLocators(androidAutomation = LocatorGroupStrategy.ALL_POSSIBLE,
            iOSXCUITAutomation = LocatorGroupStrategy.ALL_POSSIBLE)*/

    @iOSXCUITFindBy(iOSNsPredicate = "label == 'Done'")
    @WithTimeout(time = 20, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement doneBtn;
}
