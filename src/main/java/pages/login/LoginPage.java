package pages.login;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.*;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.testng.Assert;
import strategies.loginstrategies.UserNameBehaviour;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class LoginPage extends Page {

    public LoginPage(Sut sut){
        super(sut);
    }

    UserNameBehaviour loginBehaviour;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Sign in']")
    @iOSXCUITFindBy(accessibility = "Login to your account")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement loginBoxHeader;

    @AndroidFindBy(id = "email")
    @iOSXCUITFindBy(accessibility = "UserName")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement usernameField;

    @AndroidFindBy(id = "password")
    @iOSXCUITFindBy(accessibility = "Password")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement passwordField;

    @AndroidFindBy(id = "email_sign_in_button")
    @iOSXCUITFindBy(accessibility = "LoginButton")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement loginButton;

    @AndroidFindBy(id = "forgotPassword")
    @iOSXCUITFindBy(accessibility = "ForgotPasswordButton")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement forgotPasswordButton;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Token expired']")
    @iOSXCUITFindBy(id = "Token expired")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement tokenExpiryMessage;

    @AndroidFindBy(id = "common_dialog_action_button")
    @iOSXCUITFindBy(accessibility = "ReLogin")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement reloginButton;

    public void setLoginBehaviour(UserNameBehaviour loginBehaviour){
        this.loginBehaviour = loginBehaviour;
    }

   /* public void login(String username, String password){
        WaitHelper.sleep(3000);
        if (!isMailListDisplayed()){
            System.out.println("Mail list not displayed. Logging in now...");
            if (isReloginBtnDisplayed()) {
                reloginButton.click();
                WaitHelper.sleep(3000);
            }
            String usernameValue = getUsernameIfPopulated();
            System.out.println("Username value = " + usernameValue);
            if (!usernameValue.contains(TestData.USER_1_EMAIL.substring(10)) ) {
                System.out.println("Username was blank");
                usernameField.sendKeys(username);
                System.out.println("Entered "+ username +" as Username");
                passwordField.sendKeys(password);
                System.out.println("Entered " + password + " as password");
            } else {
                System.out.println("Username was NOT blank");
                passwordField.sendKeys(CredentialsHelper.getPasswordForUsername(usernameValue));
                System.out.println("Entering only password - " + CredentialsHelper.getPasswordForUsername(usernameValue));
            }
            loginButton.click();
            WaitHelper.sleep(5000);
            skipTourGuideAndGrantPermissions();
            MailListPage mailListPage = new MailListPage(driver);
            if (isAndroid()){
                WaitHelper.waitUntilPresenceOfElement(driver,30, mailListPage.inboxHeader);
            } else {
                WaitHelper.waitUntilPresenceOfElement(driver,30, mailListPage.inboxHeader);
            }

            System.out.println("Logged in...");
        } else {
            System.out.println("User is already logged in.");
        }
    }

    public void plainLogin(String username, String password){
        usernameField.sendKeys(username);
        passwordField.sendKeys(password);
        loginButton.click();
    }

    public boolean isMailListDisplayed(){
        System.out.println("Checking if the email list is already displayed.");
        System.out.println("OS = " + driver.getPlatformName());
        MailListPage mailListPage = new MailListPage(driver);
        //List<MobileElement> mailList = (new MailListPage(driver)).quickFind_emailsInList;
        boolean isDisplayed = !(mailListPage.quickFind_emailsInList.size() ==0);
        boolean isReloginButtonDisplayed = isReloginBtnDisplayed();
        System.out.println("isDisplayed = "+isDisplayed + " isReloginButtonDisplayed = " + isReloginButtonDisplayed);
        boolean listDisplayedWithoutRelogin = !isDisplayed && isReloginButtonDisplayed;

        System.out.println("listDisplayedWithoutRelogin = " + listDisplayedWithoutRelogin);
        if (isDisplayed && !isReloginButtonDisplayed){
            System.out.println("Finally returning  TRUE " );
            return true;
        } else {
            System.out.println("Finally returning  FALSE " );
            return false;
        }
    }

    public boolean isReloginBtnDisplayed(){
        System.out.println("Checking if the relogin btn is already displayed.");
        Point reloginBtn = null;
        try {
            reloginBtn = reloginButton.getCenter();
        } catch (NoSuchElementException e){
            System.out.print("Relogin btn not found.");
        }

        boolean isNotNull = !(reloginBtn == null);
        System.out.println("Relogin btn displayed = " + isNotNull);
        return isNotNull;
    }


    public String getUsernameIfPopulated(){
        return usernameField.getText();
    }

    public void verifyIfRememberMeAndSkipBtnsAreDisplayed(){
        Assert.assertTrue(dontShowAgainCheckBox.isDisplayed());
        Assert.assertTrue(skipBtn.isDisplayed());
    }

    public void swipeOnGuideScreen(MobileElement element){
        if (isiOS()){
            TouchActionsHelper.iOSSwipe(driver, "left", element);
        } else {
          *//*  Dimension d = driver.manage().window().getSize();
            int y = (int) (0.5 * (d.height));
            int x = d.width;
            Point start = new Point(((int)(0.95*x)), y);
            Point end = new Point(((int)(0.05*x)), y);
            TouchActionsHelper.swipeFromPointToPoint(driver, start, end);*//*
          *//*  TouchActions action = new TouchActions();
            action.flick(element, 1, 10, 10);
            action.perform();*//*
            TouchAction t = new TouchAction(driver);
            Dimension d = driver.manage().window().getSize();
            int xRightEdge = d.width - 5;
            int xLeftEdge = 0;
            int yMid = (int)(d.height*0.7);
            PointOption po1 = PointOption.point(xRightEdge, yMid);
            PointOption po2 = PointOption.point(xLeftEdge, yMid);
            WaitOptions w = WaitOptions.waitOptions(Duration.ofMillis(500));
            t.press(po1).waitAction(w).moveTo(po2).waitAction(w).perform();
        }
    }

    public void verifyAndroidGuidePages(){
        Assert.assertTrue(guide_screen1.isDisplayed());
        verifyIfRememberMeAndSkipBtnsAreDisplayed();
        swipeOnGuideScreen(guide_screen1);
    }

    public void skipTourGuideAndGrantPermissions(){
        try {
            dontShowAgainCheckBox.click();
            skipBtn.click();
        } catch (NoSuchElementException e){
        }
        grantPermissions();
    }

    public void grantPermissions(){
        while (getAllowPermissionButton() != null){
            driver.findElementById("com.android.packageinstaller:id/permission_allow_button").click();
            grantPermissions();
        }

    }

    public MobileElement getAllowPermissionButton(){
        MobileElement allowBtn = null;
        try {
            allowBtn = (MobileElement) driver.findElementById("com.android.packageinstaller:id/permission_allow_button");
        } catch (NoSuchElementException e){
            System.out.println("No alert message was displayed.");
        }
        return allowBtn;
    }*/
}
