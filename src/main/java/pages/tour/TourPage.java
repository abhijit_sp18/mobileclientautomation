package pages.tour;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;

import java.time.temporal.ChronoUnit;

public abstract class TourPage extends Page {

    public TourPage(Sut sut){
        super(sut);
    }

    public MobileElement pageHeading;
    public MobileElement pageDescription;
    public MobileElement pageButton;
    @AndroidFindBy(id = "dontShowAgain")
    @iOSXCUITFindBy(accessibility = "RememberMe")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement dontShowAgainCheckBox;

}
