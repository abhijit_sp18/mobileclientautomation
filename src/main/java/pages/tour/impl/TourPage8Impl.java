package pages.tour.impl;

import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import pages.tour.TourPage;

public class TourPage8Impl extends TourPage {

    @AndroidFindBy(id = "gtTitle")
    @iOSXCUITFindBy(accessibility = "TutorialTitle5")
    private MobileElement pageHeading;

    @AndroidFindBy(id = "gtDesc")
    @iOSXCUITFindBy(iOSNsPredicate = "label == 'Tutorial eight page discription'")
    private MobileElement pageDescription;

    @AndroidFindBy(id = "skipDone")
    @iOSXCUITFindBy(accessibility = "Done")
    private MobileElement pageButton;

    public TourPage8Impl(Sut sut){
        super(sut);
        super.pageHeading = this.pageHeading;
        super.pageDescription = this.pageDescription;
        super.pageButton = this.pageButton;
    }

}
