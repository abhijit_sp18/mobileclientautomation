package pages.tour.impl;

import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import pages.tour.TourPage;

public class TourPage3Impl extends TourPage {

    @AndroidFindBy(id = "gtTitle")
    @iOSXCUITFindBy(accessibility = "TutorialTitle3")
    private MobileElement pageHeading;

    @AndroidFindBy(id = "gtDesc")
    @iOSXCUITFindBy(accessibility = "Tutorial third page discription")
    private MobileElement pageDescription;

    @AndroidFindBy(id = "skipDone")
    @iOSXCUITFindBy(accessibility = "Skip")
    private MobileElement pageButton;

    public TourPage3Impl(Sut sut){
        super(sut);
        super.pageHeading = this.pageHeading;
        super.pageDescription = this.pageDescription;
        super.pageButton = this.pageButton;
    }

}
