package pages.maillists.impl;

import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.iOSXCUITBy;
import io.appium.java_client.pagefactory.iOSXCUITFindAll;
import org.openqa.selenium.By;
import pages.maillists.MailListPage;

import java.util.List;

public class MailListPageDraft extends MailListPage {

    public MailListPageDraft(Sut sut) {
        super(sut);
        super.avatarsInList = this.avatarsInList;
        super.emailsInList = this.tosInList;
        super.subjectsInList = this.subjectsList;
        super.bodiesInList = this.bodiesInList;
        super.timeStampsInList = this.timeStampInList;
        super.emailContainers = this.emailContainers;
        super.androidAvatarBy = this.androidAvatarBy;
        super.iOSAvatarBy = this.iOSAvatarBy;
        super.androidEmailBy = this.androidFromBy;
        super.iOSEmailBy = this.iOSFromBy;
        super.androidSubjectBy = this.androidSubjectBy;
        super.iOSSubjectBy = this.iOSSubjectBy;
        super.androidBodyBy = this.androidBodyBy;
        super.iOSBodyBy = this.iOSBodyBy;
        super.androidTimeStampBy = this.androidTimeStampBy;
        super.iOSTimeStampBy = this.iOSTimeStampBy;
        super.androidEmailContainerBy = this.androidEmailContainerBy;
        super.iOSEmailsContainerBy = this.iOSEmailsContainerBy;
        super.iOSEmailContainerBy = this.iOSEmailContainerBy;
    }

    public By androidAvatarBy = By.id("message_summary_user_circle");
    public By iOSAvatarBy = By.id("dummy");
    public By androidFromBy = By.id("from");
    public By iOSFromBy = By.id("dummy");
    public By androidSubjectBy = By.id("subject");
    public By iOSSubjectBy = By.id("dummy");
    public By androidBodyBy = By.id("preview");
    public By iOSBodyBy = By.id("dummy");
    public By androidTimeStampBy = By.id("received");
    public By iOSTimeStampBy = By.id("dummy");
    public By androidEmailContainerBy = By.id("swipe_reveal_layout");
    public By iOSEmailsContainerBy = By.id("dummy");
    public By iOSEmailContainerBy = By.className("dummy");

    @AndroidFindAll({
            @AndroidBy(id = "message_summary_user_circle")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(id = "dummy")
    })
    public List<MobileElement> avatarsInList;

    @AndroidFindAll({
            @AndroidBy(id = "to")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(id = "dummy")
    })
    public List<MobileElement> tosInList;

    @AndroidFindAll({
            @AndroidBy(id = "subject")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(id = "dummy")
    })
    public List<MobileElement> subjectsList;

    @AndroidFindAll({
            @AndroidBy(id = "preview")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(id = "dummy")
    })
    public List<MobileElement> bodiesInList;

    @AndroidFindAll({
            @AndroidBy(id = "sent")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(id = "dummy")
    })
    public List<MobileElement> timeStampInList;

    @AndroidFindAll({
            @AndroidBy(id = "swipe_reveal_layout")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(id = "dummy"), @iOSXCUITBy(className = "dummy")
    })
    public List<MobileElement> emailContainers;
}
