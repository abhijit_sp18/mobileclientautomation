package pages.maillists;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;

import java.util.List;

public class MailListPage extends Page {

    public MailListPage(Sut sut){
        super(sut);
    }

    public By androidAvatarBy;
    public By iOSAvatarBy;
    public By androidEmailBy;
    public By iOSEmailBy;
    public By androidSubjectBy;
    public By iOSSubjectBy;
    public By androidBodyBy;
    public By iOSBodyBy;
    public By androidTimeStampBy;
    public By iOSTimeStampBy;
    public By androidEmailContainerBy;
    public By iOSEmailsContainerBy;
    public By iOSEmailContainerBy;

    @AndroidFindBy(accessibility = "Open navigation drawer")
    @iOSXCUITFindBy(accessibility = "MenuButton")
    public MobileElement navigationDrawerBtn;
    public MobileElement pageHeading;
    @AndroidFindBy(accessibility = "Search")
    public MobileElement androidSearchBtn;
    @AndroidFindBy(accessibility = "search_src_text")
    @iOSXCUITFindBy(accessibility = "ListSearchBar")
    public MobileElement searchBar;
    @AndroidFindBy(id = "fab")
    @iOSXCUITFindBy(accessibility = "Compose Button")
    public MobileElement composeBtn;
    @iOSXCUITFindBy(accessibility = "Edit Button")
    public MobileElement iOSEditBtn;
    @iOSXCUITFindBy(accessibility = "FilterButton")
    public MobileElement iOSFilterBtn;
    @AndroidFindBy(accessibility = "More options")
    public MobileElement androidMoreOptionsBtn;
    public List<MobileElement> avatarsInList;
    public List<MobileElement> emailsInList;
    public List<MobileElement> subjectsInList;
    public List<MobileElement> bodiesInList;
    public List<MobileElement> timeStampsInList;
    public List<MobileElement> emailContainers;
}
