package pages.navvigationdrawer;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;

import java.time.temporal.ChronoUnit;

public class NavigationDrawerPage extends Page {

    public NavigationDrawerPage(Sut sut) {
        super(sut);
    }

    @AndroidFindBy(accessibility = "Sign out")
    @iOSXCUITFindBy(accessibility = "Logout Button")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement logOutBtn;

    @AndroidFindBy(id = "nav_header_profile_picture")
    @iOSXCUITFindBy(accessibility = "User profile image")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement profileBtn;

    @AndroidFindBy(id = "nav_header_name")
    @iOSXCUITFindAll({
            @iOSXCUITBy()
    })
    @iOSXCUITFindBy(iOSNsPredicate = "type == 'XCUIElementTypeStaticText' AND value CONTAINS[c] 'User name'")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement profileName;

    @AndroidFindBy(id = "nav_header_email")
    @iOSXCUITFindBy(accessibility = "ProfileUserName")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement userEmail;

    @AndroidFindBy(accessibility = "Settings")
    @iOSXCUITFindBy(accessibility = "Setting Button")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement settingsBtn;

    @AndroidFindBy(accessibility = "")
    @iOSXCUITFindBy(accessibility = "Switch Account")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement switchAccountBtn;

    @AndroidFindBy(id = "rec_nav_view")
    @iOSXCUITFindBy(accessibility = "MenuTreeView")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement foldersSection;

    @AndroidFindBy(id = "common_dialog_action_button")
    @iOSXCUITFindBy(accessibility = "OK")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement logOutConfirmation; //

    @AndroidFindBys({
            @AndroidBy(id = "rec_nav_view" ),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Inbox']")
    })
    @iOSXCUITFindBy(accessibility = "Inbox")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement inboxFolder;

    @AndroidFindBys({
            @AndroidBy(id = "rec_nav_view" ),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Sent']")
    })
    @iOSXCUITFindBy(id = "Sent")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement sentFolder;

    @AndroidFindBys({
            @AndroidBy(id = "rec_nav_view" ),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Drafts']")
    })
    @iOSXCUITFindBy(accessibility = "Drafts")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement draftsFolder;

    @AndroidFindBys({
            @AndroidBy(id = "rec_nav_view" ),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Outbox']")
    })
    @iOSXCUITFindBy(accessibility = "Outbox")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement outboxFolder;

    @AndroidFindBys({
            @AndroidBy(id = "rec_nav_view" ),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Trash']")
    })
    @iOSXCUITFindBy(accessibility = "Trash")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement trashFolder;

    @AndroidFindBy(accessibility = "Create a new folder")
    @iOSXCUITFindBy(accessibility = "Create folder button")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement createFolderBtn;

  /*  @iOSXCUITFindBy(accessibility = "Delete")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement ioS_deleteFolderOption;

    @iOSXCUITFindBys({
            @iOSXCUITBy(iOSNsPredicate = "type == 'XCUIElementTypeCell' AND label BEGINSWITH[c] 'folder name Trash'"),
            @iOSXCUITBy(id = "menu-right-arrow")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement iOS_emptyTrashBtn;*/
}
