package pages.profile;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class ProfilePage extends Page {

    public ProfilePage(Sut sut){
        super(sut);
    }

    @AndroidFindBy(accessibility = "Open navigation drawer")
    @iOSXCUITFindBy(accessibility = "Close")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement cancelBtn;

    @AndroidFindBy(accessibility = "Open navigation drawer")
    @iOSXCUITFindBy(accessibility = "Cancel")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement backBtn;

    @AndroidFindBys({
            @AndroidBy(id = "toolbar"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Profile']")
    })
    @iOSXCUITFindBy(id = "View Profile")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement screenHeading;

    @AndroidFindBy(accessibility = "Edit")
    @iOSXCUITFindBy(accessibility = "Edit")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement editBtn;

    @AndroidFindBy(id = "profile_image_holder")
    @iOSXCUITFindBy(accessibility = "profile image view")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement profileAvatar;

    @AndroidFindBy(id = "nav_header_name")
    @iOSXCUITFindBy(accessibility = "User Name")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement profileName;

    @AndroidFindBy(id = "nav_header_email")
    @iOSXCUITFindBy(accessibility = "User email id")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement userEmail;

    @AndroidFindAll({
            @AndroidBy(id = "profile_title")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public List<MobileElement> androidHeaders;

    @AndroidFindBy(id = "profile_title")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement androidWaitElement;

    @iOSXCUITFindBy(accessibility = "Firstname Field")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement iOSFirstNameValueAndHeader;

    @AndroidFindAll({
            @AndroidBy(id = "profile_content")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public List<MobileElement> androidValues;

    @iOSXCUITFindBy(accessibility = "Lastname Field")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement iOSLastNameValueAndHeader;
}
