package pages.profile;


import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class EditProfilePage extends Page {

    public EditProfilePage(Sut sut) {
        super(sut);
    }

    @AndroidFindBy(accessibility = "Open navigation drawer")
    @iOSXCUITFindBy(accessibility = "Cancel")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement cancelBtn;

    @AndroidFindBys({
            @AndroidBy(id = "toolbar"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Edit Profile']")
    })
    @iOSXCUITFindBy(id = "Edit Profile")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement heading;

    @AndroidFindBy(id = "action_save")
    @iOSXCUITFindBy(accessibility = "Save")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement saveBtn;

    @AndroidFindBy(id = "profile_circle_edit_mode")
    @iOSXCUITFindBy(accessibility = "Tap To Set Photo")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement changeProfilePictureBtn;

    @AndroidFindBy(id = "nav_header_name")
    @iOSXCUITFindBy(accessibility = "User Name")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement profileName;

    @AndroidFindBy(id = "nav_header_email")
    @iOSXCUITFindBy(accessibility = "User email id")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement emailID;

    @iOSXCUITFindBy(accessibility = "Firstname Field")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement iOSFirstNameValueAndHeader;

    @iOSXCUITFindBy(accessibility = "Lastname Field")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement iOSLastNameValueAndHeader; // androidHeaders

    @AndroidFindAll({
            @AndroidBy(id = "profile_title")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public List<MobileElement> androidHeaders;

    @AndroidFindAll({
            @AndroidBy(id = "profile_content")
    })
    public List<MobileElement> androidValues;

    @AndroidFindBys({
            @AndroidBy(id = "toolbar"), @AndroidBy(xpath = "//android.widget.TextView[@text='Edit Profile']")
    })
    @iOSXCUITFindBy(accessibility = "This field is required")
    public MobileElement errorMessage;

    public MobileElement getFirstNameField(){
        if (sut.getPlatform().equals(Sut.Platform.ANDROID)){
            return androidValues.get(0);
        } else {
            return iOSFirstNameValueAndHeader;
        }
    }

    public MobileElement getLastNameField(){
        if (sut.getPlatform().equals(Sut.Platform.ANDROID)){
            return androidValues.get(1);
        } else {
            return iOSLastNameValueAndHeader;
        }
    }
}
