package pages.compose.junk;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSXCUITBy;
import io.appium.java_client.pagefactory.iOSXCUITFindAll;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class ComposePageJunk {

   /*
    // HAS TO BE MOVED
    @AndroidFindAll({
            @AndroidBy(id = "from"),
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(id= "Title Label"),
    } )
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public List<MobileElement> emailsInList;

    // HAS TO BE MOVED
    @iOSXCUITFindBy(accessibility = "Add Picture")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement attachmentOptions_addPicture;

    // HAS TO BE MOVED
    @iOSXCUITFindBy(accessibility = "Attach From Cloud")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement attachmentOptions_addFromCloud;

    // HAS TO BE MOVED
    @iOSXCUITFindBy(accessibility = "Cancel")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement attachmentOptions_cancel;

    // HAS TO BE MOVED
    @iOSXCUITFindBy(iOSNsPredicate = "label == 'Files'")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement cloud_filesTab;

    // HAS TO BE MOVED
    @iOSXCUITFindBy(iOSNsPredicate = "label CONTAINS[c] '.jpg'")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement cloud_fileInList; */

/*
    // HAS TO BE MOVED
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "value CONTAINS[c] 'jpg'")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public List<MobileElement> cloud_filesInList;

    // HAS TO BE MOVED
    @iOSXCUITFindBy(iOSNsPredicate = "label == 'Attach as Link'")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement cloud_addAsLink;

    // HAS TO BE MOVED
    @iOSXCUITFindBy(iOSNsPredicate = "label == 'Attach as original Source/Image'")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement cloud_addAsAttachment;

    // HAS TO BE MOVED
    @iOSXCUITFindBy(iOSNsPredicate = "label == 'Cancel'")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement cloud_cancel;

    // HAS TO BE MOVED
    @iOSXCUITFindBy(accessibility = "BodyImageAttachment")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement bodyImageAttachment;

     // HAS TO BE MOVED
    @iOSXCUITFindBy(accessibility = "All albums")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement attachmentOptions_allAlbums;

    // HAS TO BE MOVED
    @iOSXCUITFindBy(accessibility = "All Photos")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement ios_allPhotos;

    // HAS TO BE MOVED
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "type == 'XCUIElementTypeCollectionView'"),
            @iOSXCUITBy(iOSNsPredicate = "type == 'XCUIElementTypeCell'")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public List<MobileElement> iOS_allPhotos_photos;

    // HAS TO BE MOVED
    @iOSXCUITFindBy(accessibility = "Attach")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement iOS_allPhotos_attachBtn;

     // HAS TO BE MOVED/REMOVED
    public Map<String, String> sendEmail(String emailID){
        Map<String, String> map =  new HashMap<String, String>();
        map = composeEmail(emailID);

        sendBtn.click();
        System.out.println("Sent mail with subject = "+ map.get(SUBJECT));
        System.out.println("Sent mail with body = "+ map.get(BODY));
        System.out.println("Sending to = "+ map.get(TO));
        return map;
    }

    // HAS TO BE MOVED/REMOVED
    public Map<String, String> composeEmail(String emailID){
        Map<String, String> map = new HashMap<>();
        map.put(TO, emailID);
        map.put(SUBJECT, StringUtils.getRandomString());
        map.put(BODY, StringUtils.getRandomString());

        toField.click();
        toField.sendKeys(map.get(TO));

        subjectField.click();
        subjectField.sendKeys(map.get(SUBJECT));

        WaitHelper.sleep(1000);
        bodyField.sendKeys(map.get(BODY));
        WaitHelper.sleep(1000);

        WebElement searchResult = getSearchResult(map.get(TO));
        if (searchResult != null) searchResult.click();
        //subjectField.click();
        return map;
    }

    // HAS TO BE MOVED/REMOVED
    public Map<String, String> composeEmail(Map<String, String> emailDetails){
        Map<String, String> map = new HashMap<>();
        map.put(TO, emailDetails.get(TO));
        map.put(SUBJECT, emailDetails.get(SUBJECT));
        map.put(BODY, emailDetails.get(BODY));

        toField.sendKeys(map.get(TO));
        System.out.println("Entered text in to");
        subjectField.click();
        System.out.println("Clicked on subject");
        subjectField.sendKeys(map.get(SUBJECT));
        System.out.println("Entered text in subject");

        WaitHelper.sleep(1000);
        bodyField.sendKeys(map.get(BODY));
        System.out.println("Entered text in body");
        WaitHelper.sleep(1000);

        WebElement searchResult = getSearchResult(map.get(TO));
        if (searchResult != null) {
            System.out.println("Search result is not null.");
            searchResult.click();
        }
        System.out.println("Search result is null. Didn't do anything.");

        System.out.println("Clicked on body");
        return map;
    }

    // HAS TO BE MOVED/REMOVED
    public Map<String, String> sendEmailWithAttachment(String email){
        addAttachmentFromDevice();
        return composeEmail(email);
    }

    // HAS TO BE MOVED/REMOVED
    public void addAttachmentFromDevice(){
        attachmentIcon.click();
        attachmentOptions_addPicture.click();
        WaitHelper.sleep(2000);
        attachmentOptions_allAlbums.click();
        WaitHelper.sleep(2000);
        ios_allPhotos.click();
        WaitHelper.sleep(2000);
        //iOS_allPhotos_photos.get(0).click();
        Dimension d = sut.getAppiumDriver().manage().window().getSize();
        Point p = new Point((int) ((d.height)*0.3), (int) ((d.width)*0.3));
        TouchActionsHelper.tapOnAnElement(sut.getAppiumDriver(),p);
        WaitHelper.sleep(2000);
        iOS_allPhotos_attachBtn.click();
        WaitHelper.sleep(3000);
    }

    // HAS TO BE MOVED/REMOVED
    public WebElement getSearchResult(String email){
        WebElement searchResult = null;
        sut.getAppiumDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        try {
            if (sut.getPlatform().equals(Sut.Platform.ANDROID)){
                searchResult = sut.getAppiumDriver().findElementById("title");

            }else {
                searchResult = sut.getAppiumDriver().findElementById(email);
            }
        } catch (NoSuchElementException e){
            System.out.println("Search results didn't appear.");
        }
        return searchResult;
    }

    // HAS TO BE MOVED/REMOVED
    public WebElement getSearchResult(){
        WebElement searchResult = null;
        sut.getAppiumDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        try {
            if (sut.getPlatform().equals(Sut.Platform.ANDROID)){
                searchResult = sut.getAppiumDriver().findElementById("title");

            }else {
                MobileElement container = (MobileElement) sut.getAppiumDriver().findElementByAccessibilityId("ContactTable");
                searchResult = container.findElementByClassName("XCUIElementTypeCell");
            }
        } catch (NoSuchElementException e){
            System.out.println("Search results didn't appear.");
        }
        return searchResult;
    }

    // HAS TO BE MOVED/REMOVED
    public Map<String, String> saveADraftEmail(String emailID){
        Map<String, String> map = new HashMap<>();
        map = composeEmail(emailID);
        cancelBtn.click();
        if (sut.getPlatform().equals(Sut.Platform.ANDROID)){
            AndroidPopupPage popupPage = new AndroidPopupPage(sut);
            popupPage.actionBtn.click();
        } else {
            IosPopupPage iOSPopupPage = new IosPopupPage(sut);
            iOSPopupPage.saveDraftBtn.click();
        }
        return map;
    }

    // HAS TO BE MOVED/REMOVED
    public void replyToMail(String reply){
        bodyField.sendKeys(reply);
        sendBtn.click();
    }

    // HAS TO BE MOVED/REMOVED
    public void forwardMail(String email){
        toField.sendKeys(email);
        WaitHelper.sleep(1500);
        WebElement results = getSearchResult();
        if (results!=null) results.click();
        WaitHelper.sleep(700);
        sendBtn.click();
    }

    // HAS TO BE MOVED/REMOVED
    public Map<String, String> sendEmptySubjectEmail(String emailID){
        Map<String, String> emailDetails = new HashMap<>();
        emailDetails.put(TO, emailID);
        emailDetails.put(SUBJECT, "");
        emailDetails.put(BODY, StringUtils.getRandomString());

        composeEmail(emailDetails);
        sendBtn.click();
        if (sut.getPlatform().equals(Sut.Platform.IOS)){
            emailDetails.put(SUBJECT, "(no subject)");
        } else {
            emailDetails.put(SUBJECT, "(No subject)");
        }
        return emailDetails;
    }

    // HAS TO BE MOVED/REMOVED
    public Map<String, String> sendEmptyBodyEmail(String emailID){
        Map<String, String> emailDetails = new HashMap<>();
        emailDetails.put(TO, emailID);
        emailDetails.put(SUBJECT, StringUtils.getRandomString());
        emailDetails.put(BODY, "");

        bodyField.clear();
        composeEmail(emailDetails);
        sendBtn.click();
        emailDetails.put(BODY, "No body text");
        return emailDetails;
    }

    */
}
