package pages.compose;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import pages.androidspecific.AndroidPopupPage;
import pages.iosspecific.IosPopupPage;
import pojos.Email;
import strategies.composestrategies.*;

import java.time.temporal.ChronoUnit;
import java.util.List;

public abstract class ComposePage extends Page {

    protected ToComposeBehaviour toComposeBehaviour;
    protected CcComposeBehaviour ccComposeBehaviour;
    protected BccComposeBehaviour bccComposeBehaviour;
    protected PriorityBehaviour priorityBehaviour;
    protected SubjectComposeBehaviour subjectComposeBehaviour;
    protected BodyComposeBehaviour bodyComposeBehaviour;
    protected AddAttachmentBehaviour attachmentBehaviour;

    public ComposePage(Sut sut) {
        super(sut);
    }

    @HowToUseLocators(androidAutomation = LocatorGroupStrategy.ALL_POSSIBLE,
            iOSXCUITAutomation = LocatorGroupStrategy.ALL_POSSIBLE)

    @AndroidFindBy(accessibility = "Navigate up")
    @iOSXCUITFindBy(accessibility = "Cancel Button")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement cancelBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='New message']")
    @iOSXCUITFindBy(accessibility = "New Message")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement newMessageTitle;

    @AndroidFindBy(accessibility = "Attach file")
    @iOSXCUITFindBy(accessibility = "Add attachment button")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement attachmentIcon;

    @AndroidFindBy(accessibility = "Send")
    @iOSXCUITFindBy(accessibility = "Send Button")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement sendBtn;

    @AndroidFindBy(accessibility = "Arrow to expand cc and bcc")
    @iOSXCUITFindBy(accessibility = "ccIdentifier")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement expandBtn;

    @AndroidFindBys({
            @AndroidBy(id = "to"),
            @AndroidBy(className = "android.widget.EditText")
    })
    @iOSXCUITFindBy(accessibility = "ToIdentifier")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement toField;

    @AndroidFindBys({
            @AndroidBy(id = "cc"),
            @AndroidBy(className = "android.widget.EditText")
    })
    @iOSXCUITFindBy(accessibility = "ccIdentifier")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement ccField;

    @AndroidFindBys({
            @AndroidBy(id = "bcc"),
            @AndroidBy(className = "android.widget.EditText")
    })
    @iOSXCUITFindBy(accessibility = "bccIdentifier")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement bccField;

    @AndroidFindBys({
            @AndroidBy(id = "compose_message_layout"),
            @AndroidBy(id = "subject")
    })
    @iOSXCUITFindBy(accessibility = "SubjectIdentifier")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement subjectField;

    @AndroidFindBy(id = "body")
    @iOSXCUITFindBy(accessibility = "Body")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement bodyField;

    @AndroidFindBy(accessibility = "Set message priority")
    @iOSXCUITFindBy(accessibility = "Priority Button")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement priorityIcon;

    @iOSXCUITFindAll({
            @iOSXCUITBy(accessibility = "MailComposeTable"),
            @iOSXCUITBy(iOSNsPredicate = "type == 'XCUIElementTypeCell'")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public List<MobileElement> attachmentsInList;

    @iOSXCUITFindAll({
            @iOSXCUITBy(accessibility = "Delete Attachment Button")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public List<MobileElement> deleteAttachmentIconsInList;

    public Email sendEmail() {
        Email email = composeEmail();
        clickButton(sendBtn);
        return email;
    }

    public  void saveEmailAsDraft(){
        boolean d = sut.getPlatform().equals(Sut.Platform.ANDROID);
        if (sut.getPlatform().equals(Sut.Platform.ANDROID)){
            saveAsDraftAndroidFlow();
        } else if (sut.getPlatform().equals(Sut.Platform.IOS)) {
            saveAsDraftIosFlow();
        }
    }

    private void saveAsDraftAndroidFlow(){
        clickButton(cancelBtn);
        AndroidPopupPage popupPage = new AndroidPopupPage(sut);
        popupPage.actionBtn.click();
    }

    private void saveAsDraftIosFlow(){
        cancelBtn.click();
        IosPopupPage iOSPopupPage = new IosPopupPage(sut);
        iOSPopupPage.saveDraftBtn.click();
    }

    public abstract Email composeEmail();

    public void setToComposeBehaviour(ToComposeBehaviour toComposeBehaviour) {
        this.toComposeBehaviour = toComposeBehaviour;
    }

    public void setCcComposeBehaviour(CcComposeBehaviour ccComposeBehaviour) {
        this.ccComposeBehaviour = ccComposeBehaviour;
    }

    public void setBccComposeBehaviour(BccComposeBehaviour bccComposeBehaviour) {
        this.bccComposeBehaviour = bccComposeBehaviour;
    }

    public void setPriorityBehaviour(PriorityBehaviour priorityBehaviour) {
        this.priorityBehaviour = priorityBehaviour;
    }

    public void setSubjectComposeBehaviour(SubjectComposeBehaviour subjectComposeBehaviour) {
        this.subjectComposeBehaviour = subjectComposeBehaviour;
    }

    public void setBodyComposeBehaviour(BccComposeBehaviour bodyComposeBehaviour) {
        this.bccComposeBehaviour = bodyComposeBehaviour;
    }

    public void setAttachmentBehaviour(AddAttachmentBehaviour attachmentBehaviour) {
        this.attachmentBehaviour = attachmentBehaviour;
    }

}
