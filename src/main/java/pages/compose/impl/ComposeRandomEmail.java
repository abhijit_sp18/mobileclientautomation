package pages.compose.impl;

import com.synchronoss.cqe.common.mobile.core.Sut;
import pages.compose.ComposePage;
import pojos.Email;
import strategies.composestrategies.impl.RandomBody;
import strategies.composestrategies.impl.RandomSubject;
import strategies.composestrategies.impl.SingleEmailToField;

import java.util.ArrayList;
import java.util.List;

public class ComposeRandomEmail extends ComposePage {

    private List<String> emailIDs;
    private String emailID;

    public ComposeRandomEmail(Sut sut, String emailID){
        super(sut);
        this.emailID = emailID;
        this.toComposeBehaviour = new SingleEmailToField(toField, emailID);
        this.subjectComposeBehaviour = new RandomSubject(subjectField);
        this.bodyComposeBehaviour = new RandomBody(bodyField);
    }

    @Override
    public Email composeEmail() {
        List<String> to = new ArrayList<String>();
        to.add(toComposeBehaviour.to());
       return new Email(to
               , subjectComposeBehaviour.subject()
               , bodyComposeBehaviour.body());
    }
}