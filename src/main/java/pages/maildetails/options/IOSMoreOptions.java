package pages.maildetails.options;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

import java.time.temporal.ChronoUnit;

public class IOSMoreOptions extends Page {

    public IOSMoreOptions(Sut sut) {
        super(sut);
    }

    @iOSXCUITFindBy(accessibility = "Spam")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement spam;

    @iOSXCUITFindBy(accessibility = "Block Sender")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement blockSender;

    @iOSXCUITFindBy(accessibility = "Print")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement print;

    @iOSXCUITFindBy(accessibility = "Unread")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement unread;

    @iOSXCUITFindBy(accessibility = "Read")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement read;

    @iOSXCUITFindBy(accessibility = "Flag")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement flag;

    @iOSXCUITFindBy(accessibility = "Unflag")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement unFlag;

    @iOSXCUITFindBy(accessibility = "Cancel")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement cancel;
}
