package pages.maildetails.options;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSXCUITBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBys;

import java.time.temporal.ChronoUnit;

public class IOSOptionsPage extends Page {

    public IOSOptionsPage(Sut sut){
        super(sut);
    }

    @iOSXCUITFindBy(accessibility = "Reply Button")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement reply;

    @iOSXCUITFindBy(accessibility = "Delete Button")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement delete;

    @iOSXCUITFindBy(accessibility = "Move To Folder Button")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement moveToFolder;

    @iOSXCUITFindBy(accessibility = "More Button")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement moreOptions;

    @iOSXCUITFindBy(accessibility = "Compose Button")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement compose;

    @iOSXCUITFindBys({
            @iOSXCUITBy(className = "XCUIElementTypeSheet"),
            @iOSXCUITBy(id = "Reply")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement iOS_combinedOptions_reply;

    @iOSXCUITFindBys({
            @iOSXCUITBy(className = "XCUIElementTypeSheet"),
            @iOSXCUITBy(accessibility = "Reply All")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement iOS_combinedOptions_replyAll;

    @iOSXCUITFindBys({
            @iOSXCUITBy(className = "XCUIElementTypeSheet"),
            @iOSXCUITBy(accessibility = "Forward")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement iOS_combinedOptions_forward;

    @iOSXCUITFindBy(accessibility = "Cancel")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement iOS_combinedOptions_cancel;

    @iOSXCUITFindBy(iOSNsPredicate = "type == 'XCUIElementTypeStaticText' AND value CONTAINS 'ead'")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement iOS_combinedOptions_readUnread;

    @iOSXCUITFindBy(iOSNsPredicate = "type == 'XCUIElementTypeStaticText' AND value CONTAINS 'lag'")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement iOS_combinedOptions_flagUnflag;

    @iOSXCUITFindBys({
            @iOSXCUITBy(className = "XCUIElementTypeSheet"),
            @iOSXCUITBy(accessibility = "Spam")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement iOS_combinedOptions_spam;

    @iOSXCUITFindBys({
            @iOSXCUITBy(className = "XCUIElementTypeSheet"),
            @iOSXCUITBy(accessibility = "Block Sender")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement iOS_combinedOptions_blockSender;

    @iOSXCUITFindBys({
            @iOSXCUITBy(className = "XCUIElementTypeSheet"),
            @iOSXCUITBy(accessibility = "Print")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement iOS_combinedOptions_print;
}
