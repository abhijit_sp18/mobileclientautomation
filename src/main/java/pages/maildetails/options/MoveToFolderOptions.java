package pages.maildetails.options;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindBys;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

import java.time.temporal.ChronoUnit;

public class MoveToFolderOptions extends Page {

    public MoveToFolderOptions(Sut sut) {
        super(sut);
    }

    @AndroidFindBys({
            @AndroidBy(id = "dialog_recycler_view"),
            @AndroidBy(xpath = "//android.widget.TextView[@text = 'Trash']")
    })
    @iOSXCUITFindBy(id = "Trash")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement trashFolder;
}
