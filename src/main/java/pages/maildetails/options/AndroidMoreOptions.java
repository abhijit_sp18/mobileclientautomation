package pages.maildetails.options;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AndroidFindBys;
import io.appium.java_client.pagefactory.WithTimeout;

import java.time.temporal.ChronoUnit;

public class AndroidMoreOptions extends Page {

    public AndroidMoreOptions(Sut sut) {
        super(sut);
    }

    @AndroidFindBy(id = "popup_menu")
    public MobileElement moreOptionsBtn;

    @AndroidFindBys({
            @AndroidBy(className = "android.widget.ListView"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Reply']")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement reply;

    @AndroidFindBys({
            @AndroidBy(className = "android.widget.ListView"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Reply all']")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement replyAll;

    @AndroidFindBys({
            @AndroidBy(className = "android.widget.ListView"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Forward']")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement forward;

    @AndroidFindBys({
            @AndroidBy(className = "android.widget.ListView"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Unread']")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement unread;

    @AndroidFindBys({
            @AndroidBy(className = "android.widget.ListView"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Read']")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement read;

    @AndroidFindBys({
            @AndroidBy(className = "android.widget.ListView"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Flag']")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement flag;

    @AndroidFindBys({
            @AndroidBy(className = "android.widget.ListView"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Unflag']")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement unFlag;

    @AndroidFindBys({
            @AndroidBy(className = "android.widget.ListView"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Delete']")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement delete;

    @AndroidFindBys({
            @AndroidBy(className = "android.widget.ListView"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Spam']")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement spam;

    @AndroidFindBys({
            @AndroidBy(className = "android.widget.ListView"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Block Sender']")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement blockSender;

    @AndroidFindBys({
            @AndroidBy(className = "android.widget.ListView"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Move']")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement move;

    @AndroidFindBys({
            @AndroidBy(className = "android.widget.ListView"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Print']")
    })
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement print;
}
