package pages.maildetails;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import pages.maildetails.options.AndroidMoreOptions;
import pages.maildetails.options.IOSMoreOptions;
import pages.maildetails.options.IOSOptionsPage;

import java.time.temporal.ChronoUnit;

public class MailDetailsPage extends Page {

    public IOSMoreOptions iosMoreOptions;
    public IOSOptionsPage iosOptionsPage;
    public AndroidMoreOptions androidMoreOptions;

    public MailDetailsPage(Sut sut){
        super(sut);
        if (sut.getPlatform().equals(Sut.Platform.IOS)){
            iosMoreOptions = new IOSMoreOptions(sut);
            iosOptionsPage = new IOSOptionsPage(sut);
        } else if (sut.getPlatform().equals(Sut.Platform.ANDROID)){
            androidMoreOptions = new AndroidMoreOptions(sut);
        }
    }

    @AndroidFindBy(accessibility = "Navigate up")
    @iOSXCUITFindBy(id = "Back Button")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement back;

    @AndroidFindBys({
            @AndroidBy(id = "toolbar"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Message details']")
    })
    @iOSXCUITFindBy(accessibility = "￼ Message details")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement heading;

    @AndroidFindBy(id = "subject")
    @iOSXCUITFindBy(id = "Subject Label")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement subject;

    @AndroidFindBy(id = "message_summary_user_circle")
    @iOSXCUITFindBy(accessibility = "Avatar Image")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement avatar;

    @AndroidFindBy(id = "from")
    @iOSXCUITFindBy(accessibility = "From Label")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement from;

    @AndroidFindBy(id = "address_line_expanded")
    @iOSXCUITFindBy(accessibility = "To Label")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement to;

    @AndroidFindBy(id = "img_expando")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement android_to_expand;

    @AndroidFindBy(id = "date_container")
    @iOSXCUITFindBy(accessibility = "Date Time Label")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement timestamp;

    @AndroidFindBys({
            @AndroidBy(id = "message_details"),
            @AndroidBy(className = "android.widget.RelativeLayout")
    })
    @iOSXCUITFindBy(accessibility = "Details Content Cell")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement bodyContainer;

    @AndroidFindBy(id = "popup_menu")
    @iOSXCUITFindBy(id = "DetailsToolBar")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement optionsContainer;

}
