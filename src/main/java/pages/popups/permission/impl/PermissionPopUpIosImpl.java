package pages.popups.permission.impl;

import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.NoSuchElementException;
import pages.popups.Popup;
import pages.popups.permission.PermissionPopUp;

import java.time.temporal.ChronoUnit;

public class PermissionPopUpIosImpl extends PermissionPopUp {

    @iOSXCUITFindBy(accessibility = "Don’t Allow")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    private MobileElement dontAllowBtn;
    @iOSXCUITFindBy(accessibility = "Allow")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    private MobileElement allowBtn;
    @iOSXCUITFindBy(accessibility = "OK")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    private MobileElement okBtn;

    public PermissionPopUpIosImpl(Sut sut) {
        super(sut);
    }

    @Override
    public void allow() {
        try {
            allowBtn.click();
        } catch (NoSuchElementException e){
            okBtn.click();
        }
    }

    @Override
    public void deny() {
        dontAllowBtn.click();
    }

    @Override
    public String getText() {
        return null;
    }

}
