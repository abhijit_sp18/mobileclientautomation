package pages.popups.permission.impl;

import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.WithTimeout;
import pages.popups.permission.PermissionPopUp;

import java.time.temporal.ChronoUnit;

public class PermissionPopUpAndroidImpl extends PermissionPopUp {

    @AndroidFindBy(id = "com.android.packageinstaller:id/permission_deny_button")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    private MobileElement dontAllowBtn;
    @AndroidFindBy(id = "com.android.packageinstaller:id/permission_allow_button")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    private MobileElement allowBtn;

    public PermissionPopUpAndroidImpl(Sut sut){ super(sut); }

    @Override
    public void allow() {
    allowBtn.click();
    }

    @Override
    public void deny() {
    dontAllowBtn.click();
    }

    @Override
    public String getText() {
        return null;
    }


}
