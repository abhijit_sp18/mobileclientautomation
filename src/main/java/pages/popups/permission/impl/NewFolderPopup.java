package pages.popups.permission.impl;

import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import pages.popups.Popup;

import java.time.temporal.ChronoUnit;

public class NewFolderPopup extends Popup {

    public NewFolderPopup(Sut sut) {
        super(sut);
        super.button1 = this.cancelBtn;
        super.button2 = this.createBtn;
        super.popupHeading = this.popupHeading;
        super.popupText = this.popupText;
    }

    @AndroidFindBy(id = "input_text_dialog_title")
    @iOSXCUITFindBy(id = "New Folder")
    public MobileElement popupHeading;

    @AndroidFindBy(id = "dummy")
    @iOSXCUITFindBy(id = "Enter folder name")
    public MobileElement popupText;

    @AndroidFindBy(id = "input")
    @iOSXCUITFindBy(id = "Folder name")
    public MobileElement enterFolderNameField;

    @AndroidFindBy(id = "dialog_negative_button")
    @iOSXCUITFindBy(accessibility = "Cancel")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    private MobileElement cancelBtn;

    @AndroidFindBy(id = "dialog_positive_button")
    @iOSXCUITFindBy(accessibility = "Create")
    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    private MobileElement createBtn;

    public void enterFolderName(String folderName){
        enterFolderNameField.sendKeys(folderName);
    }
}
