package pages.popups.permission;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;

public abstract  class PermissionPopUp extends Page {

    public PermissionPopUp(Sut sut){
        super(sut);
    }

    public abstract void allow();

    public abstract void deny();

    public abstract String getText();

    //public abstract void grantAllPermissions();

}
