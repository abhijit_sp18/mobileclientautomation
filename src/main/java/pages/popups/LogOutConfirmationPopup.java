package pages.popups;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

import java.time.temporal.ChronoUnit;

public class LogOutConfirmationPopup extends Popup {

    public LogOutConfirmationPopup(Sut sut) {
        super(sut);
        super.popupHeading = this.popupHeader;
        super.popupText = this.popupMessage;
        super.button1 = this.cancelBtn;
        super.button2 = this.okBtn;
    }

    @AndroidFindBy(id = "common_dialog_title")
    @iOSXCUITFindBy(id = "Logout")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement popupHeader;

    @AndroidFindBy(id = "common_dialog_description")
    @iOSXCUITFindBy(accessibility = "Are you sure you want to Logout?")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement popupMessage;

    @AndroidFindBy(id = "common_dialog_dismiss_button")
    @iOSXCUITFindBy(accessibility = "Cancel")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement cancelBtn;

    @AndroidFindBy(id = "common_dialog_action_button")
    @iOSXCUITFindBy(accessibility = "OK")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement okBtn;
}
