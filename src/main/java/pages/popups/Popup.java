package pages.popups;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;

public abstract class Popup extends Page {

    public MobileElement popupHeading;
    public MobileElement popupText;
    public MobileElement button1;
    public MobileElement button2;

    public Popup(Sut sut) {
        super(sut);
    }

    public MobileElement getPopupHeading(){
        return popupHeading;
    }

    public MobileElement getPopupText(){
        return popupText;
    }

    public MobileElement getPopupButton1(){
        return button1;
    }

    public MobileElement getPopupButton2(){
        return button2;
    }

    public void clickButton1(){
        getPopupButton1().click();
    }

    public void clickButton2(){
        getPopupButton2().click();
    }
}
