package pages.notifications;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.*;

import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

public class NotificationsPage extends Page {

    public NotificationsPage(Sut sut) {
        super(sut);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='SNCR Mail (QA)']")
    @iOSXCUITFindBy(accessibility = "Logout Button")
    @WithTimeout(time = 30, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement androidNotifications;

    @AndroidFindBy(id = "com.android.systemui:id/notification_title")
    @iOSXCUITFindBy(accessibility = "dummy")
    public MobileElement notificationsSender;

    public MobileElement getNotification(String content){
        if (sut.getPlatform().equals(Sut.Platform.ANDROID)){
            return androidNotifications;
        } else {
            sut.getAppiumDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            return  (MobileElement)  sut.getAppiumDriver()
                    .findElementByXPath("//XCUIElementTypeCell[contains(@label, '"+content+"')]");
        }
    }

    public void tapOnNotification(String content){
        if(sut.getPlatform().equals(Sut.Platform.ANDROID)){
            notificationsSender.click();
        } else {
            getNotification(content).click();
            ((IOSDriver)  sut.getAppiumDriver()).findElementByIosNsPredicate("value == 'notification-action-button' AND label == 'Open'").click();
        }
    }
}
