package pages.androidspecific;

import base.PageBase;
import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;

import java.time.temporal.ChronoUnit;

public class AndroidPopupPage extends PageBase {

    public AndroidPopupPage(Sut sut){
        super(sut);
    }

    @HowToUseLocators(androidAutomation = LocatorGroupStrategy.ALL_POSSIBLE,
            iOSXCUITAutomation = LocatorGroupStrategy.ALL_POSSIBLE)


    @AndroidFindBy(id = "common_dialog_action_button")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement actionBtn;

    @AndroidFindBy(id = "input")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement enterFolderNameField;

    @AndroidFindBy(id = "dialog_positive_button")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement createFolderBtn;

    @AndroidFindBy(id = "dialog_negative_button")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement cancelFolderCreationBtn;

    @AndroidFindBy(id = "common_dialog_description")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement message;

    @AndroidFindBy(id = "common_dialog_dismiss_button")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement dismissButton;

    @AndroidFindBy(id = "com.android.packageinstaller:id/permission_message")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement permissionMessage;

    @AndroidFindBy(id = "com.android.packageinstaller:id/permission_deny_button")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement denyBtn;

    @AndroidFindBy(id = "com.android.packageinstaller:id/permission_allow_button")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement allowBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='The email address you entered is not valid!']")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement invalidUsernameMessage;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Please verify your login details are correct.']")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement invalidCredentialsMessage;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Allow SNCR Mail (QA) to access your contacts?']")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement contactsPermissionMessage;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Allow SNCR Mail (QA) to access photos, media, and files on your device?']")
    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    public MobileElement mediaPermissionMessage;

}

