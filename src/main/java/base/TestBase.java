package base;

import com.synchronoss.cqe.common.mobile.base.Test;
import com.synchronoss.cqe.common.mobile.core.Sut;
import com.synchronoss.cqe.common.mobile.core.TestConfig;
import com.synchronoss.cqe.common.mobile.utils.device.DeviceUtils;
import com.synchronoss.cqe.common.mobile.utils.device.android.AdbCommandLib;
import com.synchronoss.cqe.common.report.CustomAssert;
import com.synchronoss.cqe.common.report.ReportImplementation;
import com.synchronoss.cqe.common.report.Reporter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

public class TestBase extends Test {

    public static Logger logger = LogManager.getLogger(TestBase.class.getName());

    public Sut testSut0;
    public Sut testSut1;
    public CustomAssert customAssert;

    @Override
    @BeforeSuite(alwaysRun = true)
    public void beforeSuite(ITestContext context) {
        System.out.println("Running beforeSuite");
        super.beforeSuite(context);

        CustomerConfig.loadCustomerPackageProperties();

        logger.info("beforeSuite complete");
    }

   // @Override
    @BeforeTest(alwaysRun = true)
    public void beforeTest(ITestContext context) {
        testSut0 = getSutList()[0];
        if (getSutList().length == 2) {
            testSut1 = getSutList()[1];
        }
        testSut0.setSutDescription("Copernicus Native Client Test");

        Reporter.lastFailedMessage = "";
        reporter = new Reporter(testSut0);
        customAssert = CustomAssert.getInstance();
        customAssert.setReporter(reporter);
        customAssert.setLogger(logger);

        ReportImplementation reportImplementation = new ReportImplementation();
        boolean isExtentReportNeeded = Boolean.parseBoolean(TestConfig.getTestParam(context, "report.extent.enabled"));
        if (isExtentReportNeeded) {
            reportImplementation.initExtentBeforeTest(context.getName());
        }
        /*ZephyrImplementation.initZapi(zephyrInstance, context, testSut);*/
  /*      for (Sut sut : getSutList()) {
            AppiumObject xpathObject = new AppiumObject(sut, "XPATH_REFERENCE", ElementType.IMAGE,
                    "UI Element to use as base XPath", 2,
                    GetBy.SKIP, null,
                    GetBy.XPATH, "");
            sut.setXpathObject(xpathObject);
            super.beforeTest(context);

        }*/

//testSut1.getAppiumDriver().quit();
        super.beforeTest(context);
        sleep(2000);

        for (Sut sut : getSutList()) {
            if (sut.getPlatform() == Sut.Platform.IOS) {
                logger.info("Detecting application xpath");
                DeviceUtils.safe_sleep(6000);
            }
        }

      //  Assert.assertEquals(getSutCount(), Integer.parseInt(TestConfig.getTestParam(context, "exec.sut.count")), String.format("Error, not enough devices to run the %s test",
             //   context.getName()));
    }

    @Override
    @AfterTest(alwaysRun = true)
    public void afterTest(ITestContext context) {
        super.afterTest(context);
    }

    @Override
    @AfterSuite(alwaysRun = true)
    public void afterSuite(ITestContext context) {
        super.afterSuite(context);
        logger.info("After suite complete");
    }

    @AfterMethod(groups = {TestGroups.SAMPLE})
    public void afterMethod(ITestResult iTestResult) {
        super.afterMethod(iTestResult);
        if (getSutCount() == 1) {
            if (!iTestResult.isSuccess()) {
                logger.info("Relaunching the sut0 app");
                testSut0.getAppiumDriver().launchApp();
                DeviceUtils.safe_sleep(5000);
                logger.info("Relaunching the sut0 app - Successful!");
            }
        }
        if (getSutCount() == 2) {
            if (!iTestResult.isSuccess()) {
                logger.info("Relaunching the sut0 and sut1 app");
                testSut0.getAppiumDriver().launchApp();
                testSut1.getAppiumDriver().launchApp();
                DeviceUtils.safe_sleep(5000);
                logger.info("Relaunching the sut0 and sut1 app - Successful!");
            }
        }
    }

/*    private void installQEHelper(Sut sut) {
        AdbCommandLib adbCommandLib = new AdbCommandLib(sut);
        if (!adbCommandLib.isAppInstalled(AdbCommandLib.pkg_qe_helper)) {
            boolean success = adbCommandLib.installQEHelperApp();
        }
    }*/

    protected String getAppPackage(Sut sut) {
        return sut.getAppiumDriver().getCapabilities().getCapability("appPackage").toString();
    }

    protected void navigateBack(Sut sut, int numberOfTimes) {
        while (numberOfTimes-- > 0) {
            sut.getAppiumDriver().navigate().back();
        }
    }
}
