package base;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;

public class PageBase extends Page {

    protected PageBase(Sut sut){
        super(sut);
    }

    protected boolean isAndroid(){
       return sut.getAppiumDriver().getPlatformName().equalsIgnoreCase("Android");
    }

    protected boolean isIOS(){
        return sut.getAppiumDriver().getPlatformName().equalsIgnoreCase("iOS");
    }
}
