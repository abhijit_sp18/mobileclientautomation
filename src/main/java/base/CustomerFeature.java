package base;

public class CustomerFeature {
    public String featureName;
    public String platformName;
    public String appVersion;
    public String val;

    public CustomerFeature(String _featureName, String _platformName, String _appVersion, String _val) {
        this.featureName = _featureName;
        this.platformName = _platformName;
        this.appVersion = _appVersion;
        this.val = _val;
    }
}

