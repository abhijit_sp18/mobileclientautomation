package base;

import com.synchronoss.cqe.common.mobile.base.Page;
import com.synchronoss.cqe.common.mobile.core.Sut;
import com.synchronoss.cqe.common.mobile.core.TestConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.testng.ITestContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

public class CustomerConfig {
    public static Logger logger = LogManager.getLogger(CustomerConfig.class.getName());
    public static List<CustomerFeature> customerFeatureList;

    public static void loadCustomerFeatureProperties(ITestContext context) {
        try {
            String customerFeaturePropertiesFile = getCustomerFeaturePropertiesFile(context);
            if (ClassLoader.getSystemResourceAsStream(customerFeaturePropertiesFile) != null) {
                Properties properties = new Properties();
                properties.load(ClassLoader.getSystemResourceAsStream(customerFeaturePropertiesFile));
                if (properties.size() > 0) {
                    logger.info("Customer feature properties file present");
                    for(Map.Entry<Object, Object> e : properties.entrySet()) {
                        String featureName = e.getKey().toString();
                        String value = e.getValue().toString();
                        if (!value.isEmpty() && (!featureName.isEmpty())) {
                            Page.customerPropertiesHashMap.put(featureName, value);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.catching(e);
        }

    }

    public static String getCustomerFeaturePropertiesFile(ITestContext context) {
        String customer = context.getCurrentXmlTest().getParameter("app.customer").toLowerCase();
        String platform = TestConfig.getTestParam(context,"device.os").toLowerCase();
        return String.format("%s%s.%s.features.properties", "customer-config/",customer,platform);
    }

    public static boolean isFeatureSupported(String featureName, Sut sut) {
        String platform = sut.getPlatform().toString();
        String appVersion = sut.getAppVersion().toString();

        CustomerFeature customerFeature = filterCustomerFeature(featureName,platform,appVersion);
        return customerFeature==null?false:Boolean.valueOf(customerFeature.val);
    }

    public static void loadCustomerPackageProperties() {
        try {
            String customerPackagePropertiesFile = getCustomerPackagePropertiesFile();
            JSONObject componentListEntry = new JSONObject();
            if (ClassLoader.getSystemResourceAsStream(customerPackagePropertiesFile) != null) {
                Properties properties = new Properties();
                properties.load(ClassLoader.getSystemResourceAsStream(customerPackagePropertiesFile));
                if (properties.size() > 0) {
                    logger.info("App Package properties file present");
                    TestConfig.appPackagePropertiesMap = new HashMap<String, String>();
                    for(Map.Entry<Object, Object> e : properties.entrySet()) {
                        TestConfig.appPackagePropertiesMap.put(e.getKey().toString(),e.getValue().toString());
                    }
                }
            }
        } catch (Exception e) {
            logger.catching(e);
        }
        /*try {
            String customerPackagePropertiesFile = getCustomerPackagePropertiesFile();
            JSONObject componentListEntry = new JSONObject();
            if (ClassLoader.getSystemResourceAsStream(customerPackagePropertiesFile) != null) {
                Properties properties = new Properties();
                properties.load(ClassLoader.getSystemResourceAsStream(customerPackagePropertiesFile));
                if (properties.size() > 0) {
                    logger.info("App Package properties file present");
                    TestConfig.appPackageList = new ArrayList<>();
                    for(Map.Entry<Object, Object> e : properties.entrySet()) {
                        String[] packageKey = e.getKey().toString().split("@");
                        String packageName = packageKey.length>0?packageKey[0]:e.getKey().toString();
                        String platform = packageKey.length>1?packageKey[1]:"";
                        String customerName = packageKey.length>2?packageKey[2]:"";

                        TestConfig.appPackageList.add(new AppPackage(packageName,platform,customerName,e.getValue().toString()));
                    }
                }
            }
        } catch (Exception e) {
            logger.catching(e);
        }*/
    }

    private static String getCustomerPackagePropertiesFile() {
        return "customer-config/app-packages-bundles.properties";
    }

    private static CustomerFeature filterCustomerFeature(String featureName, String platform, String appVersion) {
        String [] allVersions = appVersion.split("\\.");
        String major = allVersions.length>0?allVersions[0]:appVersion;
        String minor = allVersions.length>1?allVersions[1]:"";
        String patch = allVersions.length>2?allVersions[2]:"";
        String securityPatch = allVersions.length>3?allVersions[3]:"";
        String version = String.format("%s.%s",major,minor);

        List<CustomerFeature> customerFeatures = filterCustomerFeatures(featureName,platform,appVersion);

        if (customerFeatures == null || customerFeatures.size() == 0) {
            customerFeatures = filterCustomerFeatures(featureName,platform,"default");
        }

        CustomerFeature customerFeature = (customerFeatures != null && customerFeatures.size() >0) ? customerFeatures.get(0) : null;
        return customerFeature;
    }

    private static List<CustomerFeature> filterCustomerFeatures(String featureName, String platform, String appVersion) {
        List<CustomerFeature> customerFeatures = CustomerConfig.customerFeatureList.stream()
                .filter(c -> c.appVersion.equalsIgnoreCase(appVersion) &&
                        c.platformName.equalsIgnoreCase(platform) &&
                        c.featureName.equalsIgnoreCase(featureName))
                .collect(Collectors.toList());
        return customerFeatures;
    }
}

