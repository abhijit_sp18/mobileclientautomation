package strategies.loginstrategies.impl;

import com.synchronoss.cqe.common.mobile.base.Page;
import io.appium.java_client.MobileElement;
import strategies.loginstrategies.LoginBehaviour;

public class LoginBehaviourPlainImpl implements LoginBehaviour {

    MobileElement userName;
    MobileElement password;
    MobileElement loginButton;
    String userNameText;
    String passwordText;

    public LoginBehaviourPlainImpl(MobileElement userName, MobileElement password, MobileElement loginButton
            , String userNameText, String passwordText){
        this.userName = userName;
        this.password = password;
        this.loginButton = loginButton;
        this.userNameText = userNameText;
        this.passwordText = passwordText;
    }

    @Override
    public Page login() {
        userName.sendKeys(userNameText);
        password.sendKeys(passwordText);
        loginButton.click();
        return null; // Return the appropriate page later.
    }

}
