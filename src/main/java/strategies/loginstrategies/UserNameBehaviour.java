package strategies.loginstrategies;

import com.synchronoss.cqe.common.mobile.base.Page;

public interface UserNameBehaviour {

    public Page enterUsername();
}
