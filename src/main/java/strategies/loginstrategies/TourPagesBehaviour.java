package strategies.loginstrategies;

public interface TourPagesBehaviour {

    public void skipTourPages();
}
