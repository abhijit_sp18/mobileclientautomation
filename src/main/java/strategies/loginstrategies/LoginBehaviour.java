package strategies.loginstrategies;

import com.synchronoss.cqe.common.mobile.base.Page;

public interface LoginBehaviour {

    public Page login();
}
