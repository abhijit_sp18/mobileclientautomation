package strategies.composestrategies;

public interface PriorityBehaviour {

    enum Priorities{HIGH, MEDIUM, LOW}

    public String setPriority();
}
