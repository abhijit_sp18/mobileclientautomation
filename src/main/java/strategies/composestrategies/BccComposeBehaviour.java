package strategies.composestrategies;

import java.util.List;

public interface BccComposeBehaviour {

    public String bcc();
}
