package strategies.composestrategies;

public interface AddAttachmentBehaviour {

    public String addAttachment();
}
