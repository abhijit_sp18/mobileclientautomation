package strategies.composestrategies;

import org.openqa.selenium.WebElement;

public interface SubjectComposeBehaviour {

    public String subject();
}
