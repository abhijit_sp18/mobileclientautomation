package strategies.composestrategies;

import org.openqa.selenium.WebElement;

public interface BodyComposeBehaviour {

    public String body();
}
