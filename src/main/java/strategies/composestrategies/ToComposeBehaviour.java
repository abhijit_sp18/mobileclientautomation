package strategies.composestrategies;

import org.openqa.selenium.WebElement;

import java.util.List;

public interface ToComposeBehaviour {

    public String to();
}
