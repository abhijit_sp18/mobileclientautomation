package strategies.composestrategies.impl;

import org.openqa.selenium.WebElement;
import strategies.composestrategies.ToComposeBehaviour;

import java.util.List;

public class EmptyTo implements ToComposeBehaviour {

    WebElement toField;

    public EmptyTo(WebElement toField){
        this.toField = toField;
    }

    @Override
    public String to() {
  toField.click();
  return "";
    }
}
