package strategies.composestrategies.impl;

import org.openqa.selenium.WebElement;
import strategies.composestrategies.BodyComposeBehaviour;
import utils.StringUtils;

public class RandomBody implements BodyComposeBehaviour {

    WebElement bodyField;
    private  String randomBody;

    public RandomBody(WebElement bodyField){
        this.bodyField = bodyField;
        randomBody = StringUtils.getRandomString();
    }

    @Override
    public String body() {
        bodyField.sendKeys(randomBody);
        return randomBody;
    }
}
