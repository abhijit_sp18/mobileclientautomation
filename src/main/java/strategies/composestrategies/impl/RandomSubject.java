package strategies.composestrategies.impl;

import org.openqa.selenium.WebElement;
import strategies.composestrategies.SubjectComposeBehaviour;
import utils.StringUtils;

public class RandomSubject implements SubjectComposeBehaviour {

    WebElement subjectField;
    private  String randomText;

    public  RandomSubject(WebElement subjectField){
        this.subjectField = subjectField;
        randomText = StringUtils.getRandomString();
    }

    @Override
    public String subject() {
        subjectField.sendKeys(randomText);
        return randomText;
    }
}
