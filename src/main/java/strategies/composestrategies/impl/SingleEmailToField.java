package strategies.composestrategies.impl;

import org.openqa.selenium.WebElement;
import strategies.composestrategies.ToComposeBehaviour;

import java.util.List;

public class SingleEmailToField implements ToComposeBehaviour {

    private String emailID;
    private WebElement toField;

    public SingleEmailToField(WebElement toField, String emailID){

        this.toField = toField;
        this.emailID = emailID;
    }

    @Override
    public String to() {
        toField.click();
     toField.sendKeys(emailID);
     return emailID;
    }

}
