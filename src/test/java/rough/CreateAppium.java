package sampletests;

import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class CreateAppium {

    public static void main(String[] args) {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("automationName", "XCuiTest");
        capabilities.setCapability("platformName", "iOS");
        capabilities.setCapability("deviceName", "iPhone X");
        capabilities.setCapability("udid", "C2178F0A-1791-4A73-8F81-E5D8A0FEF2D1");
        capabilities.setCapability("bundleId", "com.synchronoss.sncr-messaging-client");
        capabilities.setCapability("xcodeorgid", "NXDDQ3VB6JD");
        capabilities.setCapability("xcodesigningid", "iPhone Developer");
        capabilities.setCapability("platformVersion", "13.3");
        capabilities.setCapability("wdaLocalPort", "8100");
        try {
            IOSDriver iosDriver = new IOSDriver(new URL("http://localhost:4723/wd/hub"), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        DesiredCapabilities capabilities2 = new DesiredCapabilities();
        capabilities2.setCapability("automationName", "XCuiTest");
        capabilities2.setCapability("platformName", "iOS");
        capabilities2.setCapability("deviceName", "iPhone X");
        capabilities2.setCapability("udid", "1244b95e3a3faef657b4be56e517bccac532fe64");
        capabilities2.setCapability("bundleId", "com.synchronoss.sncr-messaging-client");
        capabilities2.setCapability("xcodeorgid", "NXDDQ3VB6JD");
        capabilities2.setCapability("xcodesigningid", "iPhone Developer");
        capabilities2.setCapability("platformVersion", "13.1.2");
        capabilities2.setCapability("bootstrapPath", "/Users/absp0001/.npm-global/lib/node_modules/appium/node_modules/appium-webdriveragent");
        capabilities2.setCapability("agentPath", "/Users/absp0001/.npm-global/lib/node_modules/appium/node_modules/appium-webdriveragent/WebDriverAgent.xcodeproj");
        capabilities2.setCapability("wdaLocalPort", "8101");
        try {
            IOSDriver iosDriver = new IOSDriver(new URL("http://localhost:4740/wd/hub"), capabilities2);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


    }



}
