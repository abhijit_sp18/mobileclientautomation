package rough;

import actions.common.LoginActions;
import actions.common.TourActions;
import actions.iosspecific.SelectEnvironmentActions;
import base.TestBase;
import base.TestGroups;
import com.synchronoss.cqe.common.mobile.core.Sut;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.Test;
import pages.popups.permission.PermissionPopUp;
import pages.popups.permission.impl.PermissionPopUpAndroidImpl;
import pages.popups.permission.impl.PermissionPopUpIosImpl;
import pages.tour.TourPage;
import pages.tour.impl.TourPage1Impl;

import java.util.HashMap;
import java.util.Map;

public class Rough extends TestBase {

    @Test(description = "tc001", testName = "Test case 1", groups = {TestGroups.IOS_ONLY})
    public void tc001(){
        SelectEnvironmentActions selectEnvironmentActions = new SelectEnvironmentActions(testSut0);
        selectEnvironmentActions.clickDoneBtn();
        LoginActions loginActions = new LoginActions(testSut0);
        loginActions.login("testacc11@qae2e.synchronoss.net", "testacc11");
        PermissionPopUp popup = new PermissionPopUpIosImpl(testSut0);
        popup.allow();
        try {
            Thread.sleep(3000);
        }catch (Exception e){}

        TourPage tourPage = new TourPage1Impl(testSut0);
        TourActions tourActions = new TourActions(tourPage);
        tourPage = tourActions.swipeThroughAllTourPages(testSut0.getAppiumDriver());
        tourActions = new TourActions(tourPage);
        tourActions.getButton().click();
        PermissionPopUp p = new PermissionPopUpIosImpl(testSut0);
        p.allow();
    }

    @Test(description = "tc002", testName = "Test case 2", groups = {TestGroups.ANDROID_ONLY})
    public void tc002(){
        LoginActions loginActions = new LoginActions(testSut0);
        loginActions.login("testacc11@qae2e.synchronoss.net", "testacc11");

        try {
            Thread.sleep(3000);
        }catch (Exception e){}

        TourPage tourPage = new TourPage1Impl(testSut0);


        TourActions tourActions = new TourActions(tourPage);
        tourPage = tourActions.swipeThroughAllTourPages(testSut0.getAppiumDriver());
        tourActions = new TourActions(tourPage);
        tourActions.getButton().click();
        try {
            Thread.sleep(3000);
        }catch (Exception e){}
        PermissionPopUp p = new PermissionPopUpAndroidImpl(testSut0);
        p.allow();
        p.allow();
    }
    public  void iOSSwipe(Sut sut, String swipeDir, MobileElement element){
        JavascriptExecutor js = (JavascriptExecutor) testSut0.getAppiumDriver();
        Map<String, Object> params = new HashMap<>();
        params.put("direction", swipeDir);
        params.put("element", element.getId());
        js.executeScript("mobile: swipe", params);
    }
    //@Test(description = "tc002", testName = "Test case 2")
    public void tc003(){
        try {
            testSut1.getAppiumDriver().findElement(By.id("agsdgasd"));
        }catch (NoSuchElementException e) {
            reporter.fail(logger, "Element not found");
        }
    }
}